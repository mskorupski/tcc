#include "os_includes.h"
#include "Board.h"
#include <ti/sysbios/hal/Timer.h>
#include <ProtocolMSS.hpp>
#include <ultrasonicsensor.hpp>
#include <kalmanfilter.hpp>

Task_Struct taskReadProtocolStruct;
Task_Struct taskImuReadStruct;
Task_Struct makeImuGreatAgainStruct;
Task_Struct taskReadGPSStruct;
Task_Struct taskUpdateGPSStruct;
Task_Struct taskSpeedControlStruct;
char taskReadProtocolStack[DEFAULTTASKSTACKSIZE*4];
char taskImuReadStack[DEFAULTTASKSTACKSIZE];
char makeImuGreatAgainStack[DEFAULTTASKSTACKSIZE];
char taskReadGPSStack[DEFAULTTASKSTACKSIZE*2];
char taskUpdateGPSStack[DEFAULTTASKSTACKSIZE*8];
char taskSpeedControlStack[DEFAULTTASKSTACKSIZE*4];

float groundDistance_m = 0.0f;
float desiredBearing_rad = 0.0f;

GPS *gpshandler;
HBridge *motorshandler;
Protocol_MSS_Serial *protocolhandler;
IMU *imuhandler;
UltrasonicSensor *ultrahandler;
EKF *east;
EKF *north;

//KALMAN FILTER
float T = 0.2;
void f(Matrix x, Matrix u, Matrix result){
    result.arr[0] = x.arr[0] + T*x.arr[1] + T*T/2.0f*u.arr[0];
    result.arr[1] = x.arr[1] + T*u.arr[0];
}
void h(Matrix x, Matrix result){
    result.arr[0] = x.arr[0];
}
float Fx_arr[2][2] = {{1,T},{0,1}};
Matrix Fx = {2,2,(float*)Fx_arr};
float Hx_arr[1][2] = {{1,0}};
Matrix Hx = {1,2,(float*)Hx_arr};
float z_arr[1][1] = {{1}};
Matrix z = {1,1,(float*)z_arr};
float u_arr[1][1] = {{0}};
Matrix u = {1,1,(float*)u_arr};

float x_hat_arrE[2][1] = {{1},{0}};
Matrix x_hatE = {2,1,(float*)x_hat_arrE};
float P_arrE[2][2] = {{0,0},{0,0}};
Matrix PE = {2,2,(float*)P_arrE};
float Q_arrE[2][2] = {{0.1,0},{0,0.1}};
Matrix QE = {2,2,(float*)Q_arrE};
float R_arrE[1][1] = {{10}};
Matrix RE = {1,1,(float*)R_arrE};

float x_hat_arrN[2][1] = {{1},{0}};
Matrix x_hatN = {2,1,(float*)x_hat_arrN};
float P_arrN[2][2] = {{0,0},{0,0}};
Matrix PN = {2,2,(float*)P_arrN};
float Q_arrN[2][2] = {{0.1,0},{0,0.1}};
Matrix QN = {2,2,(float*)Q_arrN};
float R_arrN[1][1] = {{10}};
Matrix RN = {1,1,(float*)R_arrN};


//KALMAN FILTER

inline void Board_initAll() {
    Board_initGeneral();
    Board_initEEPROM();
//  Board_initEMAC();
    Board_initGPIO();
    Board_initI2C();
    Board_initSDSPI();
//  Board_initSPI();
    Board_initUART();
//  Board_initUSB(Board_USBDEVICE);
    Board_initUSBMSCHFatFs();
//  Board_initWatchdog();
//  Board_initWiFi();
    Board_initPWM();
}

#ifdef DEBUG_HEARTBEAT
Task_Struct heartbeatStruct;
char heartbeatStack[DEFAULTTASKSTACKSIZE];
void heartbeat(UArg arg0, UArg arg1) {
    while (1) {
        GPIO_toggle(Board_LED0);
        Task_sleep(arg0);
    }
}
#endif

void taskReadGPS(UArg arg0, UArg arg1) {
    while(1){
        gpshandler->fillUartBuffer();
        Task_sleep(10);
    }
}

void taskReadProtocol(UArg arg0, UArg arg1) {
    while(1){
        protocolhandler->receiveCommands();
        Task_sleep(100);
    }
}

void taskUpdateGPS(UArg arg0, UArg arg1) {
    Coordinate rawCoordinate_m, filteredCoordinate_dd;
    ThreeAxisMeas lastNED;
    uint32_t deltaticks;
    #ifdef DEBUG_SYSTEM_PRINTS
        Coordinate printCoordinate;
    #endif
    while(1){
        deltaticks = Clock_getTicks();
    #ifdef DEBUG_SYSTEM_PRINTS
        printCoordinate = gpshandler->getLastCoordinate();
        System_printf("Last Read:\nLa: %6f\nLo: %6f\nAl: %6f\n",printCoordinate[0],printCoordinate[1],printCoordinate[2]);
        System_flush();
        printCoordinate = gpshandler->getLastCoordinate_dd();
        System_printf("Decimal Degrees:\nLa: %6f\nLo: %6f\nAl: %6f\n\n\n",printCoordinate[0],printCoordinate[1],printCoordinate[2]);
        System_flush();
    #endif
        rawCoordinate_m = dd2m(gpshandler->getLastCoordinate_dd());
        lastNED = imuhandler->getNedAcceleration();
        z.arr[0] = rawCoordinate_m.latitude;
        u.arr[0] = lastNED.north;
        north->update(z, u);
        z.arr[0] = rawCoordinate_m.longitude;
        u.arr[0] = lastNED.east;
        east->update(z, u);
        filteredCoordinate_dd.latitude = north->getX_hat().arr[0];
        filteredCoordinate_dd.longitude = east->getX_hat().arr[0];
        filteredCoordinate_dd.altitude = rawCoordinate_m.altitude;
        filteredCoordinate_dd = m2dd(filteredCoordinate_dd);

        groundDistance_m = distance_m(protocolhandler->getDestin(),filteredCoordinate_dd);
        desiredBearing_rad = bearing_rad(protocolhandler->getDestin(),filteredCoordinate_dd) + DEG2RAD(protocolhandler->getMagneticDeclination());

        Task_sleep(arg0 - (Clock_getTicks() - deltaticks));
    }
}


void taskImuRead(UArg arg0, UArg arg1) {
    uint32_t deltaticks;
    while(!imuhandler->getInitialized()){
        Task_sleep(SECONDS2TICKS(1));
    }
     while(1){
        deltaticks = Clock_getTicks();
        imuhandler->readSensor();
        imuhandler->updateRotationQuaternion();
        Task_sleep(arg0 - (Clock_getTicks() - deltaticks));
    }
}

void makeImuGreatAgain(UArg arg0, UArg arg1){
    imuhandler->init();
    Task_exit();
}

void taskSpeedControl(UArg arg0, UArg arg1){
    bool goingForward = false;
    uint32_t deltaticks;
    while(1){
        while(protocolhandler->getManualMode()){
            Task_sleep(arg0);
        }
        deltaticks = Clock_getTicks();
        if(groundDistance_m < 1.0f) {
            motorshandler->setSpeed(0, 0);
            goto speed_sleep;
        }
        if(goingForward){
            if(ultrahandler->obstacleAhead){
                if(ultrahandler->getLastDistance_cm(0) > ultrahandler->getLastDistance_cm(2) &&
                        ultrahandler->getLastDistance_cm(1) > ultrahandler->getLastDistance_cm(2)) {
                    //obstacle if looking more to the left, go right
                    motorshandler->increaseSpeed(-3,3);
                } else if (ultrahandler->getLastDistance_cm(1) > ultrahandler->getLastDistance_cm(0) &&
                        ultrahandler->getLastDistance_cm(2) > ultrahandler->getLastDistance_cm(0)) {
                    //obstacle is looking left
                    motorshandler->increaseSpeed(-3,-3);
                } else if(ultrahandler->getLastDistance_cm(1) > ultrahandler->getLastDistance_cm(0) &&
                        ultrahandler->getLastDistance_cm(1) > ultrahandler->getLastDistance_cm(2)) {
                    //head-on obstacle, turn until get rid of the obstacle
                    goingForward = false;
                    motorshandler->getSpeed(0, 0);
                    while(ultrahandler->obstacleAhead){
                        motorshandler->setSpeed(0, 30);
                    }
                    motorshandler->setSpeed(100, 0);
                    goto speed_sleep;
                } else {
                    //tunnel, must not turn left or right
                    goto speed_sleep;
                }
            } else {
                if(ultrahandler->getLastDistance_cm(1) > 110) {
                    motorshandler->setSpeed(100, 0);
                } else {
                    motorshandler->setSpeed(ultrahandler->getLastDistance_cm(1) - 10.0, 0);
                }
            }

           if ((desiredBearing_rad - imuhandler->getHeading()) > DEG2RAD(45)) {
               goingForward = false;
               motorshandler->setSpeed(0, -25);
           } else if ((desiredBearing_rad - imuhandler->getHeading()) < DEG2RAD(-45)) {
               goingForward = false;
               motorshandler->setSpeed(0, 25);
           }
        } else {
            if(abs(desiredBearing_rad - imuhandler->getHeading()) < DEG2RAD(15)) {
                motorshandler->setSpeed(0, 0);
                goingForward = true;
            } else if(desiredBearing_rad < imuhandler->getHeading()) {
                motorshandler->setSpeed(0, 25);
            } else {
                motorshandler->setSpeed(0, -25);
            }
        }
        speed_sleep:
        Task_sleep(arg0 - (Clock_getTicks() - deltaticks));
    }
}


int main(int argc, char *argv[])
{
    Board_initAll();

    gpshandler = new GPS(Board_UART7, 57600, 200);
    motorshandler = new HBridge();
    protocolhandler = new Protocol_MSS_Serial(Board_UART5, 57600);
    imuhandler = new IMU(2);
    ultrahandler = UltrasonicSensor::getInstance();
    east = new EKF(x_hatE,u,PE,RE,QE,f,Fx,h,Hx);
    north = new EKF(x_hatN,u,PN,RN,QN,f,Fx,h,Hx);
    protocolhandler->initInterfaces(gpshandler, imuhandler, motorshandler);

    Task_Params taskParams;

#ifdef DEBUG_HEARTBEAT
    Task_Params_init(&taskParams);
    taskParams.stack = &heartbeatStack;
    taskParams.priority = 2;
    taskParams.arg0 = MILIS2TICKS(200);
    Task_construct(&heartbeatStruct, (Task_FuncPtr)heartbeat, &taskParams, NULL);
#endif

    Task_Params_init(&taskParams);
    taskParams.stack = &taskReadProtocolStack;
    taskParams.priority = 1;
    taskParams.stackSize = Task_defaultStackSize*4;
    Task_construct(&taskReadProtocolStruct, (Task_FuncPtr)taskReadProtocol, &taskParams, NULL);

    Task_Params_init(&taskParams);
    taskParams.stack = &taskImuReadStack;
    taskParams.priority = 3;
    taskParams.arg0 = MILIS2TICKS(imuhandler->refresh_ms);
    taskParams.stackSize = Task_defaultStackSize;
    Task_construct(&taskImuReadStruct, (Task_FuncPtr)taskImuRead, &taskParams, NULL);

    Task_Params_init(&taskParams);
    taskParams.stack = &makeImuGreatAgainStack;
    taskParams.priority = 10;
    taskParams.stackSize = Task_defaultStackSize;
    Task_construct(&makeImuGreatAgainStruct, (Task_FuncPtr)makeImuGreatAgain, &taskParams, NULL);

    Task_Params_init(&taskParams);
    taskParams.stack = &taskReadGPSStack;
    taskParams.priority = 1;
    taskParams.stackSize = Task_defaultStackSize*2;
    Task_construct(&taskReadGPSStruct, (Task_FuncPtr)taskReadGPS, &taskParams, NULL);

    Task_Params_init(&taskParams);
    taskParams.stack = &taskUpdateGPSStack;
    taskParams.priority = 2;
    taskParams.arg0 = MILIS2TICKS(gpshandler->refresh_ms);
    taskParams.stackSize = Task_defaultStackSize*8;
    Task_construct(&taskUpdateGPSStruct, (Task_FuncPtr)taskUpdateGPS, &taskParams, NULL);

    Task_Params_init(&taskParams);
    taskParams.stack = &taskSpeedControlStack;
    taskParams.priority = 2;
    taskParams.arg0 = MILIS2TICKS(5);
    taskParams.stackSize = Task_defaultStackSize*4;
    Task_construct(&taskSpeedControlStruct, (Task_FuncPtr)taskSpeedControl, &taskParams, NULL);


    GPIO_write(Board_LED2, Board_LED_ON);
    System_printf("Starting TI RTOS\n");System_flush();
    BIOS_start();
    for(;;);
}
