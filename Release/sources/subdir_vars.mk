################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../sources/ProtocolMSS.cpp \
../sources/cgi.cpp \
../sources/gps.cpp \
../sources/gradientdescentfilter.cpp \
../sources/hbridge.cpp \
../sources/imu.cpp \
../sources/quaternion_mss_tiny.cpp \
../sources/ultrasonicsensor.cpp 

C_SRCS += \
../sources/EK_TM4C1294XL.c 

C_DEPS += \
./sources/EK_TM4C1294XL.d 

OBJS += \
./sources/EK_TM4C1294XL.obj \
./sources/ProtocolMSS.obj \
./sources/cgi.obj \
./sources/gps.obj \
./sources/gradientdescentfilter.obj \
./sources/hbridge.obj \
./sources/imu.obj \
./sources/quaternion_mss_tiny.obj \
./sources/ultrasonicsensor.obj 

CPP_DEPS += \
./sources/ProtocolMSS.d \
./sources/cgi.d \
./sources/gps.d \
./sources/gradientdescentfilter.d \
./sources/hbridge.d \
./sources/imu.d \
./sources/quaternion_mss_tiny.d \
./sources/ultrasonicsensor.d 

OBJS__QUOTED += \
"sources\EK_TM4C1294XL.obj" \
"sources\ProtocolMSS.obj" \
"sources\cgi.obj" \
"sources\gps.obj" \
"sources\gradientdescentfilter.obj" \
"sources\hbridge.obj" \
"sources\imu.obj" \
"sources\quaternion_mss_tiny.obj" \
"sources\ultrasonicsensor.obj" 

C_DEPS__QUOTED += \
"sources\EK_TM4C1294XL.d" 

CPP_DEPS__QUOTED += \
"sources\ProtocolMSS.d" \
"sources\cgi.d" \
"sources\gps.d" \
"sources\gradientdescentfilter.d" \
"sources\hbridge.d" \
"sources\imu.d" \
"sources\quaternion_mss_tiny.d" \
"sources\ultrasonicsensor.d" 

C_SRCS__QUOTED += \
"../sources/EK_TM4C1294XL.c" 

CPP_SRCS__QUOTED += \
"../sources/ProtocolMSS.cpp" \
"../sources/cgi.cpp" \
"../sources/gps.cpp" \
"../sources/gradientdescentfilter.cpp" \
"../sources/hbridge.cpp" \
"../sources/imu.cpp" \
"../sources/quaternion_mss_tiny.cpp" \
"../sources/ultrasonicsensor.cpp" 


