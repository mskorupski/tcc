/*
 * cgi.hpp
 *
 *  Created on: 20 de fev de 2019
 *      Author: matheus
 */

#ifndef CGI_HPP_
#define CGI_HPP_

namespace web
{

class CGI
{
public:
    CGI();
    virtual ~CGI();
};

} /* namespace web */

#endif /* CGI_HPP_ */
