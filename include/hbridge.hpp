#ifndef HBRIDGE_HPP_
#define HBRIDGE_HPP_

#include <ti/drivers/PWM.h>
#include <ti/drivers/GPIO.h>
/* Board Header file */
#include "Board.h"
#pragma once


class HBridge
{
private:
    PWM_Handle pwmLeft;
    PWM_Handle pwmRight;
    PWM_Params pwmParamsLeft;
    PWM_Params pwmParamsRight;
    float trackFactor; //track is the distance between the two wheels, "Track Factor" is a multiplying factor of the angular speed
    float linearSpeed;
    float angularSpeed;

public:
    HBridge();
    virtual ~HBridge();
    int setSpeed(float linear, float angular);
    int increaseSpeed(float linear, float angular);
    int setTrackFactor(float factor);
    void stopMotors(bool left = true, bool right = true);
    void lockMotors(bool left = true, bool right = true);
    float getSpeed(bool linear, bool angular);
};

#endif /* HBRIDGE_HPP_ */
