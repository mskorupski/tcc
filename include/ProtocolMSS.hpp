/*
 * ProtocolMSS.h
 *
 *  Created on: 17 de out de 2019
 *      Author: matheus
 */

#ifndef PROTOCOLMSS_H_
#define PROTOCOLMSS_H_
#pragma once

#include <ti/drivers/UART.h>
#include <ti/drivers/SDSPI.h>
#include <ti/drivers/USBMSCHFatFs.h>
#include <sys/socket.h>
#include <gps.hpp>
#include <imu.hpp>
#include <hbridge.hpp>
#include "os_includes.h"
#include "Board.h"

extern char protocolBuff[50];

#define STX         0x02    //ASCII start of text
#define ETX         0x03    //ASCII  end  of text
#define MAX_MSG_LEN 30      //reception
const char ACK[3] = {STX, 0x06, ETX}; //ASCII ACKnoledge
const char DONE[6] = {STX,'D','O','N','E',ETX};

enum Interface_Type {
    SERIAL,
    ETHERNET,
};

enum Transport_Type {
    TCP,
    UDP,
};

enum Storage_Type {
    USBSTICK,
    SDCARD,
    LOGGING_OFF,
};

class Protocol_MSS
{
protected:
    bool manualMode;
    GPS *gpshandler;
    IMU *imuhandler;
    HBridge *hbridgehandler;
    USBMSCHFatFs_Handle usbmschfatfsHandle;
    SDSPI_Handle sdspiHandle;
    Coordinate destin;
    float magneticDeclination;
    int startLogging(Storage_Type type);
    int stopLogging();
    Task_Handle taskLogInfoHandle;
    friend void taskLogInfo(UArg arg0, UArg arg1);
public:
    Protocol_MSS();
    virtual ~Protocol_MSS();
    static Protocol_MSS* build(Interface_Type type);
    void parseCommands(const char buffer[], size_t size);
    virtual void receiveCommands(char* buffer) = 0;
    virtual void sendResponse(const char buffer[], size_t size) = 0;
    void initInterfaces(GPS *_gpshandler, IMU *_imuhandler, HBridge *_hbridgehandler);
    Coordinate getDestin();
    bool getManualMode();
    float getMagneticDeclination();
};

class Protocol_MSS_Serial: public Protocol_MSS{
private:
    UART_Params uartParams;
    UART_Handle uart;
    int uartBufferPosition;
public:
    Protocol_MSS_Serial(unsigned int Board_UARTN = Board_UART5, uint32_t baudrate = 9600);
    virtual ~Protocol_MSS_Serial();
    void receiveCommands(char* buffer = protocolBuff);
    void sendResponse(const char buffer[], size_t size);

};

class Protocol_MSS_Eth: public Protocol_MSS{
private:
    int clientfd;
    int serverfd;
    struct sockaddr_in serverAddr;
    struct sockaddr_in clientAddr;
    int optval;
    socklen_t addrlen;

public:
    Protocol_MSS_Eth(Transport_Type transportProtocol = TCP, unsigned int port = 2000);
    virtual ~Protocol_MSS_Eth();
    void receiveCommands(char* buffer = protocolBuff);
    void sendResponse(const char buffer[], size_t size);
};

#endif /* SOURCES_PROTOCOLMSS_H_ */
