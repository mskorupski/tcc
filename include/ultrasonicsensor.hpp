#ifndef ULTRASONICSENSOR_HPP_
#define ULTRASONICSENSOR_HPP_
#pragma once

#include "os_includes.h"
#include "Board.h"
#include <ti/drivers/GPIO.h>

void sensorLeftFxn(unsigned int index);
void sensorCenterFxn(unsigned int index);
void sensorRightFxn(unsigned int index);

void clk0Fxn(UArg arg0);
void clk1Fxn(UArg arg0);

#define MAORDER 5
inline float mmUpdate(float newValue);

class UltrasonicSensor
{
private:
    Clock_Struct clk0Struct;
    Clock_Struct clk1Struct;
    bool echoed[3]; //left, center, right
    float lastDistance_cm[3]; //left, center, right
    float maLeft[MAORDER]; //moving average
    float maCenter[MAORDER]; //moving average
    float maRight[MAORDER]; //moving average
    static UltrasonicSensor* instance;
    UltrasonicSensor();

public:
    static UltrasonicSensor* getInstance();
    virtual ~UltrasonicSensor();
    float getLastDistance_cm(size_t index);
    bool obstacleAhead;

    friend void clk0Fxn(UArg arg0);
    friend void sensorLeftFxn(unsigned int index);
    friend void sensorCenterFxn(unsigned int index);
    friend void sensorRightFxn(unsigned int index);
};

#endif /* ULTRASSONICSENSOR_HPP_ */
