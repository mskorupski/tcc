/*
 * os_includes.h
 *
 *  Created on: 19 de jun de 2019
 *      Author: matheus
 */

#ifndef INCLUDE_OS_INCLUDES_H_
#define INCLUDE_OS_INCLUDES_H_
#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>


#define DEFAULTTASKSTACKSIZE    512

#define TEXT2STR(text)          #text
#define TEXT2STR_M(text)        TEXT2STR(text) //with macros
#define TEXTCAT(text1,text2)    text1##text2
#define TEXTCAT_M(text1,text2)  TEXTCAT(text1,text2)
#define TICKS2MICROS(ticks)     ((ticks)*Clock_tickPeriod)
#define MICROS2TICKS(micros)    ((micros)/Clock_tickPeriod)
#define TICKS2MILIS(ticks)      ((ticks)*Clock_tickPeriod/1.0e3)
#define MILIS2TICKS(seconds)    ((seconds)*1e3/Clock_tickPeriod)
#define TICKS2SECONDS(ticks)    ((ticks)*Clock_tickPeriod/1.0e6)
#define SECONDS2TICKS(seconds)  ((seconds)*1e6/Clock_tickPeriod)
#define delay_ms(t)             Task_sleep((unsigned int)MILIS2TICKS(t))
#define RAD2DEG(rad)            ((rad)*180.0/M_PI)
#define DEG2RAD(deg)            ((deg)*M_PI/180.0)
typedef void (*voidfunction)();
typedef void (*intfunction)();

#endif /* INCLUDE_OS_INCLUDES_H_ */
