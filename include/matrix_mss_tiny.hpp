#include <math.h>
#include <stdlib.h>

struct Matrix{
    size_t r;
    size_t c;
    float* arr;
};


void matrixScalarProduct(float mult, Matrix M, Matrix MR);

void matrixScalarSumMul(float scalar, float mult, Matrix M, Matrix MR);

void matrixSum(Matrix M1, Matrix M2, Matrix MR);

void matrixSumMult(float mult1, Matrix M1, float mult2, Matrix M2, Matrix MR);

int matrixProduct(Matrix A, Matrix B, Matrix C);

float determinant(Matrix M);

void transpose(Matrix M, Matrix Mt);

int inverseMatrix(Matrix M, Matrix Mi);

void congruentMatrix(Matrix M, Matrix T, Matrix newM);
