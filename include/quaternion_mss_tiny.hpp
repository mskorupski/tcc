#ifndef QUATERNION_MSS_HPP_
#define QUATERNION_MSS_HPP_
#pragma once

#include <math.h>

struct Quaternion{
    float q0, q1, q2, q3;
    explicit operator bool() { return (q0||q1||q2||q3);}
    template <size_t N> void operator=(const float (&arr)[N]){
        if (N==3){ q0 = 0.0f; q1 = arr[0]; q2 = arr[1]; q3 = arr[2]; }
        else if(N==4){ q0 = arr[0]; q1 = arr[1]; q2 = arr[2]; q3 = arr[3]; }
    }
};

Quaternion operator+(const Quaternion& Qa, const Quaternion& Qb);

Quaternion operator+(const float& a, const Quaternion& Q);

Quaternion operator+(const Quaternion& Q, const float& a);

Quaternion operator-(const Quaternion& Qa, const Quaternion& Qb);

Quaternion operator*(const Quaternion& Qa, const Quaternion& Qb);

Quaternion operator*(const float a, const Quaternion& Q);

Quaternion operator*(const Quaternion& Q, const float a);

bool operator==(const Quaternion& Qa, const Quaternion& Qb);

Quaternion scalar(Quaternion Q);

Quaternion vector(Quaternion Q);

Quaternion conj(Quaternion Q);

float norm2(Quaternion Q);

float norm(Quaternion Q);

Quaternion versor(Quaternion Q);

Quaternion exp(Quaternion Q);

Quaternion ln(Quaternion Q);

Quaternion reciprocal(Quaternion Q);

void rotationMatrix(Quaternion Q, float* M);

#endif
