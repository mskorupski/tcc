#ifndef GPS_HPP_
#define GPS_HPP_
#pragma once

#include "os_includes.h"
#include "Board.h"
#include <string>
#include <ti/drivers/UART.h>

#define UART_BUFFER_LEN 512
#define Re 6371e3

typedef struct {
    char identifier[5];
    char fields[20][20];
    uint8_t checksum;
} NmeaSentence;

typedef union {
    float coordinate[3];
    struct {
        float latitude;
        float longitude;
        float altitude;
    };
    float operator [] (int it) const {return coordinate[it];}
    float& operator [] (int it) {return coordinate[it];}
} Coordinate;

float distance_m(const Coordinate& destin_dd, const Coordinate& origin_dd);
float bearing_rad(const Coordinate& source_dd, const Coordinate& destin_dd);

Coordinate dm2dd(const Coordinate& source_dd);
Coordinate dd2m(const Coordinate& source_dd);
Coordinate m2dd(const Coordinate& source_dd);

int parseNmeaSentence(const char buffer[], size_t size, NmeaSentence& sentence, bool leaveWhenNotCoordinate = false);


class GPS {
public:
    GPS(unsigned int Board_UARTN, uint32_t baudrate = 9600, float refresh_ms = 1000);
    virtual ~GPS();
    int update();
    Coordinate getLastCoordinate();
    Coordinate getLastCoordinate_dd(); //converted
    float getDistanceTo(Coordinate destin);
    float getBearingTo(Coordinate destin);
    bool getAvailable();
    void fillUartBuffer();
    const float refresh_ms;


private:
    bool available;
    NmeaSentence lastSentence;
    Coordinate lastCoordinate;
    UART_Params uartParams;
    UART_Handle uart;
    int uartBufferPosition;

};

#endif /* GPS_HPP_ */
