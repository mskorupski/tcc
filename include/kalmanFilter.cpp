/*
 * kalmanFilter.cpp
 *
 *  Created on: 24 de out de 2019
 *      Author: matheus
 */

#include <kalmanFilter.hpp>

EKF::EKF(Matrix &x_hat0, Matrix &u0, Matrix &P0, Matrix &R, Matrix &Q, function_2MtoM f, Matrix &Fx, function_1MtoM h, Matrix &Hx):
x_hat(x_hat0),
u(u0),
P(P0),
R(R),
Q(Q),
f(f),
Fx(Fx),
h(h),
Hx(Hx)
{
Matrix z0;
z0.c = x_hat.c;
z0.r = x_hat.r;
z0.arr = (float*)calloc(z0.r*z0.c,sizeof(float));
update(z0);
free(z0.arr);

}

EKF::~EKF()
{
    // TODO Auto-generated destructor stub
}

void EKF::update (const Matrix &new_z) {
    update(new_z,u);
}

void EKF::update (const Matrix &new_z, const Matrix &new_u) {
    static Matrix G = {x_hat.r,R.c,(float*)malloc(x_hat.r*R.c*sizeof(float))};
    static Matrix P_p = {P.r,P.c,(float*)malloc(P.r*P.c*sizeof(float))};
    static Matrix x_hat_p = {x_hat.r,x_hat.c,(float*)malloc(x_hat.r*x_hat.c*sizeof(float))};
    //_p stands for "priori" or "predict"
    static Matrix Gaux = {Hx.c,Hx.r,(float*)malloc(Hx.r*Hx.c*sizeof(float))};
    static Matrix S = {R.c,R.r,(float*)malloc(Hx.r*Hx.c*sizeof(float))};
    static Matrix y = {R.r,x_hat.c,(float*)malloc(R.r*x_hat.c*sizeof(float))};
    static Matrix Paux = {P.r,P.c,(float*)malloc(P.r*P.c*sizeof(float))};

//Prediction step
  //x_hat a priori
    f(x_hat,new_u,x_hat_p);
  //P a priori
    congruentMatrix(P,Fx,P_p);
    matrixSum(P_p,Q,P_p);

//Update step
  //Kalman Gain
    transpose(Hx,G); //G stores the transpose of Hx
    congruentMatrix(P_p,Hx,S);
    matrixSum(S,R,S);
    inverseMatrix(S,Gaux); //Gaux stores the inverse of S (innovation)
    matrixProduct(P_p,G,S); //S stores Pp*G
    matrixProduct(S,Gaux,G);
  //x_hat a posteriori
    memcpy(x_hat.arr,x_hat_p.arr,(x_hat.r*x_hat.c*sizeof(float)));
    h(x_hat_p,y);
    matrixSumMult(1,new_z,-1,y,y);
    matrixProduct(G,y,x_hat_p); //x_hat_p stores the update correction
    matrixSum(x_hat,x_hat_p,x_hat);
  //P a posteriori
    matrixProduct(G,Hx,Paux);
    matrixScalarSumMul(1,-1,Paux,Paux);
    matrixProduct(Paux,P_p,P);
}

Matrix EKF::getX_hat(){
    return x_hat;
}
