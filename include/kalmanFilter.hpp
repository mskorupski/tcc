/*
 * kalmanFilter.hpp
 *
 *  Created on: 24 de out de 2019
 *      Author: matheus
 */

#ifndef INCLUDE_KALMANFILTER_HPP_
#define INCLUDE_KALMANFILTER_HPP_

#include <matrix_mss_tiny.hpp>

typedef void (*function_1MtoM)(Matrix,Matrix);
typedef void (*function_2MtoM)(Matrix,Matrix,Matrix);

class EKF
{
private:
    Matrix x_hat;
    Matrix u;
    Matrix P;
    Matrix R;
    Matrix Q;
    Matrix Fx;
    Matrix Hx;
    function_2MtoM f;
    function_1MtoM h;

public:
    EKF(Matrix &x_hat0, Matrix &u0, Matrix &P0, Matrix &R, Matrix &Q, function_2MtoM f, Matrix &Fx, function_1MtoM h, Matrix &Hx);
    virtual ~EKF();
    void update (const Matrix &new_z);
    void update (const Matrix &new_z, const Matrix &new_u);
    Matrix getX_hat();
};

#endif /* INCLUDE_KALMANFILTER_HPP_ */
