#ifndef INCLUDE_IMU_HPP_
#define INCLUDE_IMU_HPP_
#pragma once

#define G 9.807f
//#define G 9.77f

#include <ti/drivers/I2C.h>
#include <mpu9250registers.h>
#include <quaternion_mss_tiny.hpp>

#include "os_includes.h"
#include "Board.h"

// wake on motion
uint8_t _womThreshold;
// scale factors
float accSens;
float gyroSens;
float magSens_x, magSens_y, magSens_z;
const float tempSens = 333.87f; //Sensitivity
const float roomTempOffset = 21.0f;
uint8_t _srd;
int __accelRange;
int __gyroRange;
int __bandwidth;

typedef union {
    struct {
        float x;
        float y;
        float z;
    };
    struct{
        float north;
        float east;
        float down;
    };
    struct{
        float roll;
        float pitch;
        float yaw;
    };

    float xyz[3];
    float operator [] (int it) const {return xyz[it];}
    float& operator [] (int it) {return xyz[it];}
    void operator=(const Quaternion& Q){x=Q.q1;y=Q.q2;z=Q.q3;}
} ThreeAxisMeas;

typedef union{
    struct{
        ThreeAxisMeas accelerometer;
        ThreeAxisMeas gyroscope;
        ThreeAxisMeas magnetometer;
    };
    ThreeAxisMeas agm[3];
    ThreeAxisMeas operator [] (int it) const {return agm[it];}
    ThreeAxisMeas& operator [] (int it) {return agm[it];}
} ImuSensorsMeas;

uint8_t         txBuffer[2];
uint8_t         rxBuffer[21];

void imuGradientDescent(const ImuSensorsMeas &newMeas, Quaternion& q, float Ts, float beta = 31.6f);
ThreeAxisMeas q2ned(const Quaternion& q);
ThreeAxisMeas q2euler(const Quaternion& q);


class IMU
{
public:
    IMU(float sampleRate);
    virtual ~IMU();
    ImuSensorsMeas getLastMeasurement();
    Quaternion getRotationQuaternion();
    ThreeAxisMeas getNedAcceleration(); //North = x, East = y, Down = -z
    ThreeAxisMeas getEulerAngles();
    float getLastTemperature();

    void init();
    int readSensor();
    void updateRotationQuaternion();
    bool getInitialized();
    float getHeading();

    int setSrd(uint8_t srd);
    int setAccelRange(int range);
    int setGyroRange(int range);
    int setDlpfBandwidth(int bandwidth);
    int calibrateGyroscope();
    int calibrateAccelerometer();
    int calibrateMagnetometer();

    const float refresh_ms;
    Quaternion rotationQuaternion;
private:

    int readRegisters(uint8_t subAddress, uint8_t count, uint8_t* dest);
    int writeRegister(uint8_t subAddress, uint8_t data);
    int writeAK8963Register(uint8_t subAddress, uint8_t data);
    int readAK8963Registers(uint8_t subAddress, uint8_t count, uint8_t* dest);
    int readAccelerometerRaw(float xyz[3]);
    int readGyroscopeRaw(float xyz[3]);
    int readMagnetometerRaw(float xyz[3]);

    bool initialized;
    float lastTemperature;
    ImuSensorsMeas lastMeasurement;
    ThreeAxisMeas gyroBias;
    ThreeAxisMeas accelBias;
    ThreeAxisMeas magBias;
    ThreeAxisMeas accelScaleFactor;
    ThreeAxisMeas magScaleFactor;
    //    Quaternion rotationQuaternion;

    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

    float gyroBias_x, gyroBias_y, gyroBias_z;
    float accelBias_x, accelBias_y, accelBias_z;
    float accelScaleFactor_x, accelScaleFactor_y, accelScaleFactor_z;
    float magBias_x, magBias_y, magBias_z;
    float magScaleFactor_x, magScaleFactor_y, magScaleFactor_z;

};

#endif /* INCLUDE_IMU_HPP_ */
