#include <quaternion_mss_tiny.hpp>

Quaternion operator+(const Quaternion& Qa, const Quaternion& Qb) {
    Quaternion Qret;
    Qret.q0 = Qa.q0 + Qb.q0;
    Qret.q1 = Qa.q1 + Qb.q1;
    Qret.q2 = Qa.q2 + Qb.q2;
    Qret.q3 = Qa.q3 + Qb.q3;
    return Qret;
}
Quaternion operator+(const float& a, const Quaternion& Q) {
    Quaternion Qret;
    Qret.q0 = Q.q0 + a;
    Qret.q1 = Q.q1;
    Qret.q2 = Q.q2;
    Qret.q3 = Q.q3;
    return Qret;
}
Quaternion operator+(const Quaternion& Q, const float& a) {return a+Q;}

Quaternion operator-(const Quaternion& Qa, const Quaternion& Qb) {
    Quaternion Qret;
    Qret.q0 = Qa.q0 - Qb.q0;
    Qret.q1 = Qa.q1 - Qb.q1;
    Qret.q2 = Qa.q2 - Qb.q2;
    Qret.q3 = Qa.q3 - Qb.q3;
    return Qret;
}

Quaternion operator*(const Quaternion& Qa, const Quaternion& Qb) {
    Quaternion Qret;
    Qret.q0 = Qa.q0*Qb.q0 - Qa.q1*Qb.q1 - Qa.q2*Qb.q2 - Qa.q3*Qb.q3;
    Qret.q1 = Qa.q0*Qb.q1 + Qa.q1*Qb.q0 + Qa.q2*Qb.q3 - Qa.q3*Qb.q2;
    Qret.q2 = Qa.q0*Qb.q2 - Qa.q1*Qb.q3 + Qa.q2*Qb.q0 + Qa.q3*Qb.q1;
    Qret.q3 = Qa.q0*Qb.q3 + Qa.q1*Qb.q2 - Qa.q2*Qb.q1 + Qa.q3*Qb.q0;
    return Qret;
}

Quaternion operator*(const float a, const Quaternion& Q) {
    Quaternion Qret;
    Qret.q0 = Q.q0*a;
    Qret.q1 = Q.q1*a;
    Qret.q2 = Q.q2*a;
    Qret.q3 = Q.q3*a;
    return Qret;
}
Quaternion operator*(const Quaternion& Q, const float a) {return a*Q;}


bool operator==(const Quaternion& Qa, const Quaternion& Qb) {
    return (Qa.q0==Qb.q0 && Qa.q1==Qb.q1 && Qa.q2==Qb.q2 && Qa.q3==Qb.q3);
    }


Quaternion scalar(Quaternion Q){
    Quaternion Qret;
    Qret.q0 = Q.q0;
    Qret.q1 = 0;
    Qret.q2 = 0;
    Qret.q3 = 0;
    return Qret;
}

Quaternion vector(Quaternion Q){
    Quaternion Qret;
    Qret.q0 = 0;
    Qret.q1 = Q.q1;
    Qret.q2 = Q.q2;
    Qret.q3 = Q.q3;
    return Qret;
}

Quaternion conj(Quaternion Q){
    Quaternion Qret;
    Qret.q0 = Q.q0;
    Qret.q1 = Q.q1*-1;
    Qret.q2 = Q.q2*-1;
    Qret.q3 = Q.q3*-1;
    return Qret;
}

float norm2(Quaternion Q){
    return (Q.q0*Q.q0+Q.q1*Q.q1+Q.q2*Q.q2+Q.q3*Q.q3);
}

float norm(Quaternion Q){
    return sqrt(norm2(Q));
}

Quaternion versor(Quaternion Q){
    Quaternion Qret;
    float n = norm(Q);
    Qret.q0 = Q.q0/n;
    Qret.q1 = Q.q1/n;
    Qret.q2 = Q.q2/n;
    Qret.q3 = Q.q3/n;
    return Qret;
}


Quaternion exp(Quaternion Q){
    return (versor(vector(Q))*sin(norm(Q) + cos(norm(Q))))*exp(Q.q0);
}

Quaternion ln(Quaternion Q){
    return versor(vector(Q))*acos(Q.q0/norm(Q)) + log(norm(Q));
}

Quaternion reciprocal(Quaternion Q){
    return conj(Q)*(1/norm2(Q));
}

void rotationMatrix(Quaternion Q, float* M){
    (*(M+0*3+0)) = 1 - 2*Q.q2*Q.q2 - 2*Q.q3*Q.q3;
    (*(M+0*3+1)) =     2*Q.q1*Q.q2 - 2*Q.q3*Q.q0;
    (*(M+0*3+2)) =     2*Q.q1*Q.q3 + 2*Q.q2*Q.q0;
    (*(M+1*3+0)) =     2*Q.q1*Q.q2 + 2*Q.q3*Q.q0;
    (*(M+1*3+1)) = 1 - 2*Q.q1*Q.q1 - 2*Q.q3*Q.q3;
    (*(M+1*3+2)) =     2*Q.q2*Q.q3 - 2*Q.q1*Q.q0;
    (*(M+2*3+0)) =     2*Q.q1*Q.q3 - 2*Q.q2*Q.q0;
    (*(M+2*3+1)) =     2*Q.q2*Q.q3 + 2*Q.q1*Q.q0;
    (*(M+2*3+2)) = 1 - 2*Q.q1*Q.q1 - 2*Q.q2*Q.q2;
}//matrix_mss_tiny.hpp compliant
