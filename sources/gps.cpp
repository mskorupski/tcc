#include <gps.hpp>
#include <math.h>
#include <errno.h>

char uartBuffer[UART_BUFFER_LEN];

void GPS::fillUartBuffer(){
    if(uartBufferPosition >= UART_BUFFER_LEN) {
        uartBufferPosition = 0;
        return;
    }
    int ret = UART_read(uart,(uartBuffer+uartBufferPosition),1);
    if(ret > 0){
        if (uartBufferPosition > 5) {
            if(uartBuffer[uartBufferPosition] == '\n'){
                if(uartBuffer[uartBufferPosition-1] == '\r' && \
                   uartBuffer[0] == '$'){
                    if (!parseNmeaSentence(uartBuffer,uartBufferPosition,lastSentence, true)) {
                        update();
                    }
                }
                uartBufferPosition = 0;
                return;
            }
        }
        uartBufferPosition += ret;
    }
}

float distance_m(const Coordinate& destin_dd, const Coordinate& origin_dd) {
    //Using havesine formula
    float la1 = DEG2RAD(origin_dd.latitude);
    float la2 = DEG2RAD(destin_dd.latitude);
    float Dla = la2-la1;
    float Dlo = DEG2RAD((destin_dd[1]-origin_dd[1]));
    float a = sin(Dla/2)*sin(Dla/2) + cos(la1)*cos(la2)*sin(Dlo/2)*sin(Dlo/2);
    return Re*2*atan2(sqrt(a),sqrt(1-a));
}

float bearing_rad(const Coordinate& destin_dd, const Coordinate& origin_dd) {
    float originLa = DEG2RAD(origin_dd.latitude);
    float destinLa = DEG2RAD(destin_dd.latitude);
    float deltaLon = DEG2RAD((destin_dd.longitude - origin_dd.longitude));
    return atan2(cos(destinLa)* sin(deltaLon),
                 cos(originLa)*sin(destinLa)-sin(originLa)*cos(destinLa)*cos(deltaLon));
}

Coordinate dd2m(const Coordinate& coord) {
    Coordinate ret;
    float a = cos(DEG2RAD(coord[0]))*sin(DEG2RAD(coord[1]/2));
    ret[0] = Re*DEG2RAD(coord[0]);
    ret[1] = Re*2*atan2(a,sqrt(1-a*a));
    ret[2] = coord[2];
    return ret;
}

Coordinate m2dd(const Coordinate& coord) {
    Coordinate ret;
    float a = tan(coord[1]/2/Re);
    ret[0] = RAD2DEG(coord[0]/Re);
    ret[1] = RAD2DEG(2*asin(a/cos(coord[0]/Re)/sqrt(a*a+1)));
    ret[2] = coord[2];
    return ret;
}

Coordinate dm2dd(const Coordinate& coord) {
    Coordinate ret;
    ret.latitude = (float)(int)(coord.latitude/100);
    ret.latitude += (coord.latitude - 100*ret.latitude)/60;
    ret.longitude = (float)(int)(coord.longitude/100);
    ret.longitude += (coord.longitude- 100*ret.longitude)/60;
    ret.altitude = coord.altitude;
    return ret;
}

int parseNmeaSentence(const char buffer[], size_t size, NmeaSentence& sentence, bool leaveWhenNotCoordinate){
    int it = 0, jt = 0, kt = 0;
    char identifier[5];
    if(size < 5) {
        return EINVAL;
    }
    if(buffer[it] == '$' && buffer[it+6] == ',') {
        if(leaveWhenNotCoordinate){
            identifier[0] = buffer[++it];
            identifier[1] = buffer[++it];
            identifier[2] = buffer[++it];
            identifier[3] = buffer[++it];
            identifier[4] = buffer[++it];
            if(strncmp(identifier, "GPGGA", 5) && \
               strncmp(identifier, "GPRMA", 5) && \
               strncmp(identifier, "GPRMC", 5)){
                return EAGAIN;
            } else {
                strncpy(sentence.identifier, identifier,5);
            }
        } else {
        sentence.identifier[0] = buffer[++it];
        sentence.identifier[1] = buffer[++it];
        sentence.identifier[2] = buffer[++it];
        sentence.identifier[3] = buffer[++it];
        sentence.identifier[4] = buffer[++it];
        }
        it+=2;
    } else {return -1;}
    while(buffer[it] != '\0' && it<size){
        if(buffer[it]==','){
            jt++;
            it++;
            kt = 0;
        }
        else if (buffer[it] == '*' && \
                  buffer[it+3] == '\r' && \
                  buffer[it+4] == '\n') {
           if(buffer[it+1] >= 'a') {
               sentence.checksum=16*(buffer[it+1]-'a' + 10);
           } else if(buffer[it+1] >= 'A') {
               sentence.checksum=16*(buffer[it+1]-'A' + 10);
           } else {
               sentence.checksum=16*(buffer[it+1]-'0');
           }
           if(buffer[it+2] >= 'a') {
               sentence.checksum+=(buffer[it+2]-'a' + 10);
           } else if(buffer[it+2] >= 'A') {
               sentence.checksum+=(buffer[it+2]-'A' + 10);
           } else {
               sentence.checksum+=(buffer[it+2]-'0');
           }
           return 0;
       } else {
            sentence.fields[jt][kt++] = buffer[it++];
       }
    }
    return -1;
}


int GPS::update() {
    uartBufferPosition = 0;

    /*
    $GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
    0    = UTC of Position
    1    = Latitude
    2    = N or S
    3    = Longitude
    4    = E or W
    5    = GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)
    6    = Number of satellites in use [not those in view]
    7    = Horizontal dilution of position
    8    = Antenna altitude above/below mean sea level (geoid)
    9    = Meters  (Antenna height unit)
    10   = Geoidal separation (Diff. between WGS-84 earth ellipsoid and
           mean sea level.  -=geoid is below WGS-84 ellipsoid)
    11   = Meters  (Units of geoidal separation)
    12   = Age in seconds since last update from diff. reference station
    13   = Diff. reference station ID#
    14   = Checksum
    */
    if (!strncmp(lastSentence.identifier,"GPGGA",5)) {
        if(lastSentence.fields[1][0] && lastSentence.fields[3][0]) {
            if (lastSentence.fields[2][0] == 'N') {
                lastCoordinate.latitude = atof(lastSentence.fields[1]);
            } else if (lastSentence.fields[2][0] == 'S') {
                lastCoordinate.latitude = -1 * atof(lastSentence.fields[1]);
            } else {
                return -1;
            }
            if (lastSentence.fields[4][0] == 'E') {
                lastCoordinate.longitude = atof(lastSentence.fields[3]);
            } else if (lastSentence.fields[4][0] == 'W') {
                lastCoordinate.longitude = -1 * atof(lastSentence.fields[3]);
            } else {
                return -1;
            }
            lastCoordinate.altitude = atof(lastSentence.fields[8]);
            available = true;
            memset(lastSentence.identifier,0,sizeof(lastSentence.identifier));
            memset(lastSentence.fields, 0, sizeof(lastSentence.fields[0][0]) * 20 * 20);//lastReceived.clear();
            return 0;
        }
        available = false;

    /*
    $GPRMA,A,llll.ll,N,lllll.ll,W,,,ss.s,ccc,vv.v,W*hh
    0    = Data status
    1    = Latitude
    2    = N/S
    3    = longitude
    4    = W/E
    5    = not used
    6    = not used
    7    = Speed over ground in knots
    8    = Course over ground
    9   = Variation
    10   = Direction of variation E/W
    11   = Checksum
    */
    } else if (!strncmp(lastSentence.identifier,"GPRMA",5)) {
        if(lastSentence.fields[1][0] && lastSentence.fields[3][0]) {
            if (lastSentence.fields[2][0] == 'N') {
                lastCoordinate.latitude = atof(lastSentence.fields[1]);
            } else if (lastSentence.fields[2][0] == 'S') {
                lastCoordinate.latitude = -1 * atof(lastSentence.fields[1]);
            } else {
                return -1;
            }
            if (lastSentence.fields[4][0] == 'E') {
                lastCoordinate.longitude = atof(lastSentence.fields[3]);
            } else if (lastSentence.fields[4][0] == 'W') {
                lastCoordinate.longitude = -1 * atof(lastSentence.fields[3]);
            } else {
                return -1;
            }
            available = true;
            memset(lastSentence.identifier,0,sizeof(lastSentence.identifier));
            memset(lastSentence.fields, 0, sizeof(lastSentence.fields[0][0]) * 20 * 20);//lastReceived.clear();
            return 0;
        }
        available = false;

    /*
    $GPRMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,ddmmyy,x.x,a*hh
    0    = UTC of position fix
    1    = Data status (V=navigation receiver warning)
    23    = Latitude of fix
    3    = N or S
    4    = Longitude of fix
    5    = E or W
    6    = Speed over ground in knots
    7    = Track made good in degrees True
    8    = UT date
    9   = Magnetic variation degrees (Easterly var. subtracts from true course)
    10   = E or W
    11   = Checksum
     */
    } else if (!strncmp(lastSentence.identifier,"GPRMC",5)) {
        if(lastSentence.fields[2][0] && lastSentence.fields[4][0]) {
            if (lastSentence.fields[3][0] == 'N') {
                lastCoordinate.latitude = atof(lastSentence.fields[2]);
            } else if (lastSentence.fields[3][0] == 'S') {
                lastCoordinate.latitude = -1 * atof(lastSentence.fields[2]);
            } else {
                return -1;
            }
            if (lastSentence.fields[5][0] == 'E') {
                lastCoordinate.longitude = atof(lastSentence.fields[4]);
            } else if (lastSentence.fields[5][0] == 'W') {
                lastCoordinate.longitude = -1 * atof(lastSentence.fields[4]);
            } else {
                return -1;
            }
            available = true;
            memset(lastSentence.identifier,0,sizeof(lastSentence.identifier));
            memset(lastSentence.fields, 0, sizeof(lastSentence.fields[0][0]) * 20 * 20);
            return 0;
        }
        available = false;
    }
    available = false;
    memset(lastSentence.identifier,0,sizeof(lastSentence.identifier));
    memset(lastSentence.fields, 0, sizeof(lastSentence.fields[0][0]) * 20 * 20);//lastReceived.clear();
    return 0;
}


GPS::GPS(unsigned int Board_UARTN, uint32_t baudrate, float refresh_ms): refresh_ms(refresh_ms) {
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = baudrate;
    uartParams.readTimeout = 2;
    uart = UART_open(Board_UARTN, &uartParams);
#ifdef DEBUG_SYSTEM_PRINTS
    if (!uart) {
        System_printf("UART@GPS did not open\n");
    } else {
        System_printf("UART@GPS is open\n");
    }
    System_flush();
#endif
    lastCoordinate.latitude = 0.0;
    lastCoordinate.longitude= 0.0;
    lastCoordinate.altitude = 0.0;
    memset(uartBuffer, 0, sizeof(uartBuffer));
    uartBufferPosition = 0;

}

GPS::~GPS() {
    // TODO Auto-generated destructor stub
}

Coordinate GPS::getLastCoordinate() {
    return lastCoordinate;
}

Coordinate GPS::getLastCoordinate_dd() {
    return dm2dd(lastCoordinate);
}

bool GPS::getAvailable(){
    return available;
}
