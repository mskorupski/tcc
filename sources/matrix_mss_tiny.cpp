/*
 * matrix_mss_tiny.cpp
 *
 *  Created on: 28 de out de 2019
 *      Author: matheus
 */

#include <matrix_mss_tiny.hpp>

void matrixScalarProduct(float mult, float* M, size_t r, size_t c, float* MR){
    for (int it = 0; it<r*c; ++it){
        (*(MR+it)) = mult*(*(M+it));
    }
}
void matrixScalarProduct(float mult, Matrix M, Matrix MR){
    matrixScalarProduct(mult,M.arr,M.r,M.c,MR.arr);
}

void matrixScalarSumMul(float scalar, float mult, float* M, size_t r, size_t c, float* MR){
    for (int it = 0; it<r*c; ++it){
        (*(MR+it)) = scalar + mult*(*(M+it));
    }
}
void matrixScalarSumMul(float scalar, float mult, Matrix M, Matrix MR){
    matrixScalarSumMul(scalar, mult, M.arr, M.r, M.c, MR.arr);
}

void matrixSum(float* M1, size_t r, size_t c, float* M2, float* MR){
    for (int it = 0; it<r*c; ++it){
        (*(MR+it)) = (*(M1+it)) + (*(M2+it));
    }
}
void matrixSum(Matrix M1, Matrix M2, Matrix MR){
    matrixSum(M1.arr,M1.r,M1.c,M2.arr,MR.arr);
}

void matrixSumMult(float mult1, float* M1, size_t r, size_t c, float mult2, float* M2, float* MR){
    for (int it = 0; it<r*c; ++it){
        (*(MR+it)) = mult1*(*(M1+it)) + mult2*(*(M2+it));
    }
}
void matrixSumMult(float mult1, Matrix M1, float mult2, Matrix M2, Matrix MR){
    matrixSumMult(mult1,M1.arr,M1.r,M1.c, mult2, M2.arr, MR.arr);
}

 //r = rows, c = columns
int matrixProduct(float* M1, size_t r1, size_t c1,  float* M2, size_t r2, size_t c2, float* MR){
    if (c1 != r2){
        return -1;
    }
    for (int it = 0; it < r1; ++it){
        for (int jt = 0; jt < c2; ++jt){
            *(MR+it*c1+jt) = 0;
            for (int kt = 0; kt < r2; ++kt){
                *(MR+it*c2+jt) += *(M1+it*c1+kt) * *(M2+kt*c2+jt);
            }
        }
    }
    return 0;
}
int matrixProduct(Matrix A, Matrix B, Matrix C){
    if(C.c!=B.c || C.r!=A.r){
        return -1;
    } else {
        return matrixProduct(A.arr, A.r, A.c, B.arr, B.r, B.c, C.arr);
    }
}


float determinant2x2(float* M){
    return ( ((*(M+0*2+0)) * (*(M+1*2+1))) - ((*(M+0*2+1)) * (*(M+1*2+0))) );
}
float determinant3x3(float* M){
    return (((*(M+0*3+0)) * (*(M+1*3+1)) * (*(M+2*3+2))) + \
            ((*(M+0*3+1)) * (*(M+1*3+2)) * (*(M+2*3+0))) + \
            ((*(M+0*3+2)) * (*(M+1*3+0)) * (*(M+2*3+1))) - \
            ((*(M+2*3+0)) * (*(M+1*3+1)) * (*(M+0*3+2))) - \
            ((*(M+2*3+1)) * (*(M+1*3+2)) * (*(M+0*3+0))) - \
            ((*(M+2*3+2)) * (*(M+1*3+0)) * (*(M+0*3+1))));
}
float determinant(Matrix M){
    if(M.r!=M.c || M.r <= 0){
        return -111.111111;
    }
    switch(M.r){
        case 1:
            return (*(M.arr));
        case 2:
            return determinant2x2(M.arr);
        case 3:
            return determinant3x3(M.arr);
        default:
            return -111.111111;
    }
}

void transpose(float* M, size_t r, size_t c, float* Mt){
    for (int it = 0; it<r; ++it){
        for (int jt = 0; jt<c; ++jt){
            (*(Mt+it*r+jt)) = (*(M+jt*c+it));
        }
    }
}
void transpose(Matrix M, Matrix Mt){
    if(M.c==Mt.r && M.r==Mt.c){
        transpose(M.arr,M.r,M.c,Mt.arr);
    }
}

void inverseMatrix2x2(float* M, float* Mi){
    float det = determinant2x2(M);
    (*(Mi+0*2+0)) =    (*(M+1*2+1))/det;
    (*(Mi+0*2+1)) = -1*(*(M+0*2+1))/det;
    (*(Mi+1*2+0)) = -1*(*(M+1*2+0))/det;
    (*(Mi+1*2+1)) =    (*(M+0*2+0))/det;
}
void inverseMatrix3x3(float* M, float* Mi){
    float det = determinant3x3(M);
    (*(Mi+0*3+0)) = ((*(M+1*3+1))*(*(M+2*3+2)) - (*(M+1*3+2))*(*(M+2*3+1)))/det;
    (*(Mi+1*3+0)) = ((*(M+1*3+2))*(*(M+2*3+0)) - (*(M+1*3+0))*(*(M+2*3+2)))/det;
    (*(Mi+2*3+0)) = ((*(M+1*3+0))*(*(M+2*3+1)) - (*(M+1*3+1))*(*(M+2*3+0)))/det;
    (*(Mi+0*3+1)) = ((*(M+0*3+2))*(*(M+2*3+1)) - (*(M+0*3+1))*(*(M+2*3+2)))/det;
    (*(Mi+1*3+1)) = ((*(M+0*3+0))*(*(M+2*3+2)) - (*(M+0*3+2))*(*(M+2*3+0)))/det;
    (*(Mi+2*3+1)) = ((*(M+0*3+1))*(*(M+2*3+0)) - (*(M+0*3+0))*(*(M+2*3+1)))/det;
    (*(Mi+0*3+2)) = ((*(M+0*3+1))*(*(M+1*3+2)) - (*(M+0*3+2))*(*(M+1*3+1)))/det;
    (*(Mi+1*3+2)) = ((*(M+0*3+2))*(*(M+1*3+0)) - (*(M+0*3+0))*(*(M+1*3+2)))/det;
    (*(Mi+2*3+2)) = ((*(M+0*3+0))*(*(M+1*3+1)) - (*(M+0*3+1))*(*(M+1*3+0)))/det;
}
int inverseMatrix(Matrix M, Matrix Mi){
    if(M.r!=M.c || M.r!=Mi.r || M.c!=Mi.c){
        return -111.111111;
    } else if(M.r <= 0){
        return -111.111111;
    }
    switch(M.r){
        case 1:
            (*(Mi.arr)) = (*(M.arr));
            return 0;
        case 2:
            inverseMatrix2x2(M.arr, Mi.arr);
            return 0;
        case 3:
            inverseMatrix3x3(M.arr, Mi.arr);
            return 0;
        default:
            return -111.111111;
    }
}

int congruentMatrix(float* M, int rM, int cM, float* T, int rT, int cT, float* newM){
    int ret;
#ifndef __c99
        float* auxT = (float*)malloc(cT*rM*sizeof(float));
        float* Tt = (float*)malloc(cT*rT*sizeof(float));
#endif
#ifdef __c99 //if the C dialect is C99, use VLA instead of malloc
        float auxT[cM*rM];
        float Tt[cM*rM];
#endif
        ret = matrixProduct(T,rT,cT,M,rM,cM,auxT);
        transpose(T,rT,cT,Tt);
        ret |= matrixProduct(auxT,rT,cT,Tt,rT,cT,newM);
#ifndef __c99
        free(auxT);
        free(Tt);
#endif
    return ret;
}
void congruentMatrix(Matrix M, Matrix T, Matrix newM){
    congruentMatrix(M.arr,M.r,M.c,T.arr,T.r,T.c,newM.arr);
}
