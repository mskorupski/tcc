#include <hbridge.hpp>

HBridge::HBridge()
{
    trackFactor = 1.0f;
    linearSpeed = 0.0f;
    angularSpeed = 0.0f;
    PWM_Params_init(&pwmParamsLeft);
    PWM_Params_init(&pwmParamsRight);
    pwmParamsLeft.period = 20000;
    pwmParamsRight.period = 20000;
    pwmLeft =  PWM_open(Board_PWM1, &pwmParamsLeft);
    pwmRight = PWM_open(Board_PWM2, &pwmParamsRight);
    PWM_setDuty(pwmLeft,0);
    PWM_setDuty(pwmRight,0);

}

HBridge::~HBridge()
{
    // TODO Auto-generated destructor stub
}


int HBridge::setSpeed(float linear, float angular){
    if((abs(linear) + abs(angular)) > 100.0) {
        return -1;
    }
    float leftSpeed, rightSpeed;
    linearSpeed = linear;
    angularSpeed = angular;

    if(!linear && !angular){
        leftSpeed = 0;
        rightSpeed = 0;
    } else {
//        leftSpeed = (linear - angular*trackFactor)/(abs(linear)+abs(angular*trackFactor))*pwmParamsLeft.period;
//        rightSpeed = (linear + angular*trackFactor)/(abs(linear)+abs(angular*trackFactor))*pwmParamsRight.period;
        leftSpeed = (linear - angular*trackFactor)/100*pwmParamsLeft.period;
        rightSpeed = (linear + angular*trackFactor)/100*pwmParamsRight.period;

    }
    GPIO_write(Board_GPIO_L0, leftSpeed<0);
    GPIO_write(Board_GPIO_L1, leftSpeed>0);
    PWM_setDuty(pwmLeft, abs(leftSpeed));

    GPIO_write(Board_GPIO_L2, rightSpeed>0);
    GPIO_write(Board_GPIO_L3, rightSpeed<0);
    PWM_setDuty(pwmRight, abs(rightSpeed));

    return 0;
}

int HBridge::increaseSpeed(float linear, float angular){
    return setSpeed(linear + linearSpeed, angular + angularSpeed);
}

int HBridge::setTrackFactor(float factor){
    if(factor < 0){
        return -1;
    }
    trackFactor = factor;
    return 0;
}

void HBridge::stopMotors(bool left, bool right){
    if(left){
        GPIO_write(Board_GPIO_L0,0);
        GPIO_write(Board_GPIO_L1,0);
        PWM_setDuty(pwmLeft,0);
    }
    if(right){
        GPIO_write(Board_GPIO_L2,0);
        GPIO_write(Board_GPIO_L3,0);
        PWM_setDuty(pwmRight,0);
    }
}

void HBridge::lockMotors(bool left, bool right){
    if(left){
        GPIO_write(Board_GPIO_L0,1);
        GPIO_write(Board_GPIO_L1,1);
        PWM_setDuty(pwmLeft,0);
    }
    if(right){
        GPIO_write(Board_GPIO_L2,1);
        GPIO_write(Board_GPIO_L3,1);
        PWM_setDuty(pwmRight,0);
    }
}

float HBridge::getSpeed(bool linear, bool angular){
    if (linear and not angular){
        return linearSpeed;
    } else if (angular and not linear){
        return angularSpeed;
    } else {
        return 0.0;
    }
}
