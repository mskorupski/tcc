#include <ultrasonicsensor.hpp>

uint32_t ticksTrigger;

inline float ticks2cm(uint32_t ticks){
    return ((TICKS2MICROS(ticks)-20)*17/1000.0);
}
//-20 is to compensate the 20us delay from the optocoupler
//17 is due 340m/s, but is twice the distance, which ends up being the same as half the speed
///1000 is magnitude order conversion


UltrasonicSensor* UltrasonicSensor::instance = nullptr;

UltrasonicSensor* UltrasonicSensor::getInstance() {
    if (instance == nullptr) {
        instance = new UltrasonicSensor();
    }
    return instance;
}

UltrasonicSensor::UltrasonicSensor()
{
    for (int it = 0; it < MAORDER; ++it) {
        maLeft[it] = 0;
        maCenter[it] = 0;
        maRight[it] = 0;
    }
    for (int it = 0; it < 3; ++it) {
        lastDistance_cm[it]=0.0;
        echoed[it]=false;
    }
    GPIO_setCallback(Board_GPIO_N2, sensorLeftFxn);
    GPIO_enableInt(Board_GPIO_N2);
    GPIO_setCallback(Board_GPIO_N3, sensorCenterFxn);
    GPIO_enableInt(Board_GPIO_N3);
    GPIO_setCallback(Board_GPIO_P2, sensorRightFxn);
    GPIO_enableInt(Board_GPIO_P2);

    Clock_Params clkParams;
    Clock_Params_init(&clkParams);
    clkParams.period = MICROS2TICKS(30e3);
    clkParams.startFlag = TRUE;
    Clock_construct(&clk0Struct, (Clock_FuncPtr)clk0Fxn,
                    1, &clkParams);

    Clock_Params clkParams2;
    Clock_Params_init(&clkParams2);
    clkParams2.period = clkParams.period;
    clkParams2.startFlag = TRUE;
    Clock_construct(&clk1Struct, (Clock_FuncPtr)clk1Fxn,
                    2, &clkParams);
}

UltrasonicSensor::~UltrasonicSensor()
{
    // TODO Auto-generated destructor stub
}
float UltrasonicSensor::getLastDistance_cm(size_t index){
    return lastDistance_cm[index];
}


void clk0Fxn(UArg arg0)
{
    UltrasonicSensor::getInstance()->obstacleAhead =
            (UltrasonicSensor::getInstance()->lastDistance_cm[0] < 25.0f) ||
            (UltrasonicSensor::getInstance()->lastDistance_cm[1] < 25.0f) ||
            (UltrasonicSensor::getInstance()->lastDistance_cm[2] < 25.0f);
    GPIO_write(Board_GPIO_H3,1);
    for (int it = 0; it<3; ++it){
        if(!UltrasonicSensor::getInstance()->echoed[it]){
//            UltrasonicSensor::getInstance()->lastDistance_cm[it] =
//                    ((30e3-20)*17/1000.0);
            }
        UltrasonicSensor::getInstance()->echoed[it] = false;
    }
}
void clk1Fxn(UArg arg0)
{
    GPIO_write(Board_GPIO_H3,0);
    ticksTrigger = Clock_getTicks();
}

void sensorLeftFxn(unsigned int index)
{
    UltrasonicSensor::getInstance()->echoed[0] = true;
    UltrasonicSensor::getInstance()->lastDistance_cm[0] +=
            (ticks2cm(Clock_getTicks() - ticksTrigger) - UltrasonicSensor::getInstance()->maLeft[MAORDER-1])/MAORDER;
    for (int it = MAORDER; it-->1; UltrasonicSensor::getInstance()->maLeft[it]=UltrasonicSensor::getInstance()->maLeft[it-1]);
    UltrasonicSensor::getInstance()->maLeft[0] =
            ticks2cm(Clock_getTicks() - ticksTrigger);
#ifdef DEBUG_SYSTEM_PRINTS
    System_printf("left: %d ticks\n", Clock_getTicks() - trigTime);
    System_flush();
#endif
}

void sensorCenterFxn(unsigned int index)
{
    UltrasonicSensor::getInstance()->echoed[1] = true;
    UltrasonicSensor::getInstance()->lastDistance_cm[1] +=
            (ticks2cm(Clock_getTicks() - ticksTrigger) - UltrasonicSensor::getInstance()->maCenter[MAORDER-1])/MAORDER;
    for (int it = MAORDER; it-->1; UltrasonicSensor::getInstance()->maCenter[it]=UltrasonicSensor::getInstance()->maCenter[it-1]);
    UltrasonicSensor::getInstance()->maCenter[0] =
            ticks2cm(Clock_getTicks() - ticksTrigger);
#ifdef DEBUG_SYSTEM_PRINTS
    System_printf("center: %d ticks\n", Clock_getTicks() - trigTime);
    System_flush();
#endif
}

void sensorRightFxn(unsigned int index)
{
    UltrasonicSensor::getInstance()->echoed[2] = true;
    UltrasonicSensor::getInstance()->lastDistance_cm[2] +=
            (ticks2cm(Clock_getTicks() - ticksTrigger) - UltrasonicSensor::getInstance()->maRight[MAORDER-1])/MAORDER;
    for (int it = MAORDER; it-->1; UltrasonicSensor::getInstance()->maRight[it]=UltrasonicSensor::getInstance()->maRight[it-1]);
    UltrasonicSensor::getInstance()->maRight[0] =
            ticks2cm(Clock_getTicks() - ticksTrigger);
#ifdef DEBUG_SYSTEM_PRINTS
    System_printf("right: %d ticks\n", Clock_getTicks() - trigTime);
    System_flush();
#endif
}


