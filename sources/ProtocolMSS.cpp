/*
 * ProtocolMSS.cpp
 *
 *  Created on: 17 de out de 2019
 *      Author: matheus
 */

#include <ProtocolMSS.hpp>
#include <cstring>

char protocolBuff[50];


Protocol_MSS::Protocol_MSS() {
    manualMode = true;
    destin.latitude = 90.0f;
    destin.longitude = 0.0f;
    taskLogInfoHandle = NULL;
    magneticDeclination = 20;
}

Protocol_MSS::~Protocol_MSS()
{
    // TODO Auto-generated destructor stub
}

Protocol_MSS* Protocol_MSS::build(Interface_Type type){
    if (type== SERIAL){
        return new Protocol_MSS_Serial();
    } else if (type == ETHERNET){
        return new Protocol_MSS_Eth();
    } else return nullptr;
}

bool Protocol_MSS::getManualMode(){
    return manualMode;
}

Coordinate Protocol_MSS::getDestin(){
    return destin;
}

float Protocol_MSS::getMagneticDeclination(){
    return magneticDeclination;
}

void Protocol_MSS::initInterfaces(GPS *_gpshandler, IMU *_imuhandler, HBridge *_hbridgehandler){
    gpshandler = _gpshandler;
    imuhandler = _imuhandler;
    hbridgehandler = _hbridgehandler;
}

void Protocol_MSS::parseCommands(const char buffer[], size_t size){
    char response[50];
    if (size < 6 or size > MAX_MSG_LEN){
        sendResponse("\x02INVALID\x03",9);
        return;
    }

    #ifdef DEBUG_SYSTEM_PRINTS
        System_printf("Command Received: %s",buffer);
        System_flush();
    #endif

    if (!strncmp(buffer+1, "GET", 3) and buffer[4] == ' ') {
        if(!strncmp((buffer+5),"COORD_DD",8)){
            System_sprintf(response,"\x02%11.4f %11.4f %11.4f\x03",
                    gpshandler->getLastCoordinate_dd()[0],
                    gpshandler->getLastCoordinate_dd()[1],
                    gpshandler->getLastCoordinate_dd()[2]
                    );
        }else if(!strncmp((buffer+5),"FIX",3)){
            System_sprintf(response,"\x02%d\0x03",gpshandler->getAvailable());
        }else if(!strncmp((buffer+5),"SPEED",5)){
            System_sprintf(response,"\x02%f %f\x03",
                    hbridgehandler->getSpeed(true, false),
                    hbridgehandler->getSpeed(false, true)
                    );
        }else if(!strncmp((buffer+5),"TEMP",4)){
            System_sprintf(response,"\x02%6.2f\x03",imuhandler->getLastTemperature());
        }else if(!strncmp((buffer+5),"COORD",5)){
            System_sprintf(response,"\x02%11.4f %11.4f %11.4f\x03",
                    gpshandler->getLastCoordinate()[0],
                    gpshandler->getLastCoordinate()[1],
                    gpshandler->getLastCoordinate()[2]
                    );
        }else if(!strncmp((buffer+5),"NED",3)){
            System_sprintf(response,"\x02%5.2f %5.2f %5.2f\x03",
                    imuhandler->getNedAcceleration()[0],
                    imuhandler->getNedAcceleration()[1],
                    imuhandler->getNedAcceleration()[2]
                    );
        }else if(!strncmp((buffer+5),"ACC",3)){
            System_sprintf(response,"\x02%5.2f %5.2f %5.2f\x03",
                    imuhandler->getLastMeasurement().accelerometer[0],
                    imuhandler->getLastMeasurement().accelerometer[1],
                    imuhandler->getLastMeasurement().accelerometer[2]
                    );
        }else if(!strncmp((buffer+5),"MAG",3)){
            System_sprintf(response,"\x02%5.2f %5.2f %5.2f\x03",
                    imuhandler->getLastMeasurement().magnetometer[0],
                    imuhandler->getLastMeasurement().magnetometer[1],
                    imuhandler->getLastMeasurement().magnetometer[2]
                    );
        }else if(!strncmp((buffer+5),"GYR",3)){
            System_sprintf(response,"\x02%5.2f %5.2f %5.2f\x03",
                    imuhandler->getLastMeasurement().gyroscope[0],
                    imuhandler->getLastMeasurement().gyroscope[1],
                    imuhandler->getLastMeasurement().gyroscope[2]
                    );
        }else {
            sendResponse("\x02INVALID\x03",9);
            return;
        }
        sendResponse(response,strlen(response)+1);
    #ifdef DEBUG_SYSTEM_PRINTS
        System_printf("Response Sent: %s",response);
        System_flush();
    #endif
        return;
    } else if (!strncmp(buffer+1, "SET", 3) and buffer[4] == ' '){
        if(!strncmp((buffer+5),"DEST",4) and buffer[9]==' '){
            sendResponse(ACK,3);
            sscanf(buffer+9,"%f %f",destin.coordinate,destin.coordinate+1);
            sendResponse(ACK,3);
        }else if(!strncmp((buffer+5),"MAN",3) and buffer[8]==' '){
            sendResponse(ACK,3);
            if (manualMode ^ (buffer[9] != '0')){
                hbridgehandler->setSpeed(0.0,0.0);
                manualMode = (buffer[9] != '0');
            }
        }else if(!strncmp((buffer+5),"TFACT",5) and buffer[10]==' '){ //track factor
            hbridgehandler->setTrackFactor(atof(buffer+11));
        }else if (!strncmp((buffer+5),"SPEED",5) and buffer[10]==' '){
            if(!manualMode){
                sendResponse("\x02NON MANUAL\x03",12);
                return;
            } else {
                float spdaux[2] = {0.0,0.0};
                sendResponse(ACK,3);
                sscanf(buffer+11,"%f %f",spdaux,spdaux+1);
                hbridgehandler->setSpeed(spdaux[0], spdaux[1]);
            }
        } else if(!strncmp((buffer+5),"CALIB",5) and buffer[10]==' '){
            if (!strncmp((buffer+11),"ACC",3)) {
                sendResponse(ACK,3);
                hbridgehandler->setSpeed(0.0,0.0);
                if(!manualMode){
                    manualMode = true;
                    imuhandler->calibrateAccelerometer();
                    manualMode = false;
                } else {
                    imuhandler->calibrateAccelerometer();
                }
            } else if (!strncmp((buffer+11),"GYR",3)) {
                sendResponse(ACK,3);
                hbridgehandler->setSpeed(0.0,0.0);
                if(!manualMode){
                    manualMode = true;
                    imuhandler->calibrateGyroscope();
                    manualMode = false;
                } else {
                    imuhandler->calibrateGyroscope();
                }
            } else if (!strncmp((buffer+11),"MAG",3)) {
                sendResponse(ACK,3);
                hbridgehandler->setSpeed(0.0,0.0);
                if(!manualMode){
                    manualMode = true;
                    imuhandler->calibrateMagnetometer();
                    manualMode = false;
                } else {
                    imuhandler->calibrateMagnetometer();
                }
            }
        } else if(!strncmp((buffer+5),"LOG",3) and buffer[8]==' '){
            if (!strncmp((buffer+9),"USB",3)) {
                sendResponse(ACK,3);
                startLogging(USBSTICK);
            } else if (!strncmp((buffer+9),"SD",2)) {
                sendResponse(ACK,3);
                startLogging(SDCARD);
            } else if (!strncmp((buffer+9),"OFF",3)) {
                sendResponse(ACK,3);
                stopLogging();
            }
        } else {
            sendResponse("\x02INVALID\x03",9);
            return;
        }
        sendResponse(DONE,6);
        return;
    }
    sendResponse("\x02INVALID\x03",9);
    return;
}

Protocol_MSS_Serial::Protocol_MSS_Serial(unsigned int Board_UARTN, uint32_t baudrate){
    uartBufferPosition = 0;
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = baudrate;
//    uartParams.readTimeout = 2;
    uart = UART_open(Board_UARTN, &uartParams);
#ifdef DEBUG_SYSTEM_PRINTS
    if (!uart) {
        System_printf("UART@ProtocolMSS did not open\n");
    } else {
        System_printf("UART@ProtocolMSS is open\n");
    }
    System_flush();
#endif
}
Protocol_MSS_Serial::~Protocol_MSS_Serial(){

}

void Protocol_MSS_Serial::receiveCommands(char* buffer){
    if (uartBufferPosition >= 50 || uartBufferPosition < 0){
        uartBufferPosition = 0;
        return;
    }
    int ret = UART_read(uart,(buffer+uartBufferPosition),1);
    if (ret>0){
        if(buffer[uartBufferPosition] == ETX){
            if(buffer[0] == STX){
                parseCommands(buffer,uartBufferPosition);
            }
            uartBufferPosition = 0;
        } else {
            uartBufferPosition += ret;
        }
    }
    return;
}

void Protocol_MSS_Serial::sendResponse(const char buffer[], size_t size){
    if (buffer == nullptr || size < 2 || size>50){
        return;
    }
    UART_write(uart, buffer, size);
    return;
}

Protocol_MSS_Eth::Protocol_MSS_Eth(Transport_Type transportProtocol, unsigned int port){
    int status;
    addrlen = sizeof(clientAddr);
    if(transportProtocol == TCP){
        serverfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    } else if(transportProtocol == UDP) {
        serverfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    } else {
        serverfd = -1;
    }

    if (serverfd == -1) {
        System_abort("Socket not created @ Protocol_MSS\n");
    }

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(port);

    optval = 1;
    if (setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0) {
        System_abort("setsockopt failed @ Protocol_MSS\n");
    }

    status = bind(serverfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (status == -1) {
        System_abort("Socket not binded @ Protocol_MSS\n");
    }

    status = listen(serverfd, 3);
    if (status == -1) {
        System_abort("Port couldn't be listened @ Protocol_MSS\n");
    }

}
Protocol_MSS_Eth::~Protocol_MSS_Eth(){

}

void Protocol_MSS_Eth::receiveCommands(char* buffer){
    int bytesRcvd;
    while((clientfd = accept(serverfd, (struct sockaddr *)&clientAddr, &addrlen)) != -1){
        while ((bytesRcvd = recv(clientfd, buffer, MAX_MSG_LEN, 0)) > 0) {
            if((buffer[bytesRcvd-1] == ETX) && (buffer[0] == STX)){
                parseCommands(buffer,bytesRcvd);
            }
            return;
        }
        return;
    }
    return;
}

void Protocol_MSS_Eth::sendResponse(const char buffer[], size_t size){
    if (buffer == nullptr || size < 2 || size>50){
        return;
    }
    send(clientfd, buffer, size, 0);
    return;
}

static uint8_t usbServiceTaskStack[1024];
char taskLogInfoStack[DEFAULTTASKSTACKSIZE*8];
void taskLogInfo(UArg arg0, UArg arg1) {
    FILE *f;
    char filename[19];
    float info[3];
    Protocol_MSS *protocolhandler = (Protocol_MSS*)arg1;
    while(1) {
        System_sprintf(filename,"fat:0:log%05d.txt",(int)TICKS2SECONDS(Clock_getTicks()));
        f = fopen(filename, "w");
        if (!f) {
            System_printf("Error opening \"%s\"\n", filename); System_flush();
            Task_sleep(SECONDS2TICKS(60));
            continue;
        }
        memcpy(info,protocolhandler->gpshandler->getLastCoordinate().coordinate,sizeof(info));
        fprintf(f,"{\n");
        fprintf(f,"  \"Last Raw Coordinate\": {\n"
                  "    \"latitude\": {\n"
                  "      \"value\": %11.4f,\n"
                  "      \"unit\": \"dd\"\n"
                  "    },\n"
                  "    \"longitude\": {\n"
                  "      \"value\": %11.4f,\n"
                  "      \"unit\": \"dd\"\n"
                  "    },\n"
                  "    \"altitude\": {\n"
                  "      \"value\": %11.2f,\n"
                  "      \"unit\": \"m\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);

        //memcpy(info,filteredcoordinate,sizeof(info));
        fprintf(f,"  \"Last Filtered Coordinate\": {\n"
                  "    \"latitude\": {\n"
                  "      \"value\": %11.4f,\n"
                  "      \"unit\": \"dd\"\n"
                  "    },\n"
                  "    \"longitude\": {\n"
                  "      \"value\": %11.4f,\n"
                  "      \"unit\": \"dd\"\n"
                  "    },\n"
                  "    \"altitude\": {\n"
                  "      \"value\": %11.2f,\n"
                  "      \"unit\": \"m\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);

        memcpy(info,protocolhandler->destin.coordinate,sizeof(info));
        fprintf(f,"  \"Destination\": {\n"
                 "    \"latitude\": {\n"
                 "      \"value\": %11.4f,\n"
                 "      \"unit\": \"dd\"\n"
                 "    },\n"
                 "    \"longitude\": {\n"
                 "      \"value\": %11.4f,\n"
                 "      \"unit\": \"dd\"\n"
                 "    },\n"
                 "    \"altitude\": {\n"
                 "      \"value\": %11.2f,\n"
                 "      \"unit\": \"m\"\n"
                 "    }\n"
                 "  },\n",info[0],info[1],info[2]);

        fprintf(f,"  \"fix\": %s,\n",(protocolhandler->gpshandler->getAvailable()? "true" : "false"));

        memcpy(info,protocolhandler->imuhandler->getLastMeasurement().accelerometer.xyz,sizeof(info));
        fprintf(f,"  \"Accelerometer\": {\n"
                  "    \"x\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    },\n"
                  "    \"y\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    },\n"
                  "    \"z\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);

        memcpy(info,protocolhandler->imuhandler->getLastMeasurement().gyroscope.xyz,sizeof(info));
        fprintf(f,"  \"Gyroscope\": {\n"
                  "    \"x\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad/s\"\n"
                  "    },\n"
                  "    \"y\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad/s\"\n"
                  "    },\n"
                  "    \"z\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad/s\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);
        memcpy(info,protocolhandler->imuhandler->getLastMeasurement().magnetometer.xyz,sizeof(info));
        fprintf(f,"  \"Magnetometer\": {\n"
                  "    \"x\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"uT\"\n"
                  "    },\n"
                  "    \"y\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"uT\"\n"
                  "    },\n"
                  "    \"z\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"uT\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);
        memcpy(info,protocolhandler->imuhandler->getNedAcceleration().xyz,sizeof(info));
        fprintf(f,"  \"NED Acceleration\": {\n"
                  "    \"N\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    },\n"
                  "    \"E\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    },\n"
                  "    \"D\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"m/s2\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);
        memcpy(info,protocolhandler->imuhandler->getEulerAngles().xyz,sizeof(info));
        fprintf(f,"  \"Euler Angles\": {\n"
                  "    \"yaw\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad\"\n"
                  "    },\n"
                  "    \"pitch\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad\"\n"
                  "    },\n"
                  "    \"roll\": {\n"
                  "      \"value\": %6.2f,\n"
                  "      \"unit\": \"rad\"\n"
                  "    }\n"
                  "  },\n",info[0],info[1],info[2]);

        fprintf(f,"  \"Temperature\": {\n"
                  "    \"value\": %6.2f,\n"
                  "    \"unit\": \"C\"\n"
                  "  },\n",protocolhandler->imuhandler->getLastTemperature());

        fprintf(f,"  \"Speed\": {\n"
                  "    \"linear\": {\n"
                  "      \"value\": %5.1f,\n"
                  "      \"unit\": \"%%\"\n"
                  "    },\n"
                  "    \"angular\": {\n"
                  "      \"value\": %5.1f,\n"
                  "      \"unit\": \"%%\"\n"
                  "    }\n"
                  "  }\n"
                  "}",
                  protocolhandler->hbridgehandler->getSpeed(true, false),
                  protocolhandler->hbridgehandler->getSpeed(false, true));

        fflush(f);
        fclose(f);
        Task_sleep(arg0);
    }
}

int Protocol_MSS::startLogging(Storage_Type type) {
    static SDSPI_Params sdspiParams;
    static USBMSCHFatFs_Params usbmschfatfsParams;
    static Task_Params taskParams;
    if(taskLogInfoHandle != NULL) {
        if(Task_getMode(taskLogInfoHandle) != Task_Mode_TERMINATED) {
            return -1;
        }
    }
    switch (type) {
        case USBSTICK:
            USBMSCHFatFs_Params_init(&usbmschfatfsParams);
            usbmschfatfsParams.serviceTaskStackPtr = usbServiceTaskStack;
            usbmschfatfsParams.serviceTaskStackSize = sizeof(usbServiceTaskStack);
            usbmschfatfsHandle = USBMSCHFatFs_open(Board_USBMSCHFatFs0,
                                                   0,
                                                  &usbmschfatfsParams);
            if (usbmschfatfsHandle == NULL) {
                System_printf("Error starting the USB Drive\n"); System_flush();
                return -1;
            }
            else {
                System_printf("USB Drive mounted\n"); System_flush();
            }

            if (!USBMSCHFatFs_waitForConnect(usbmschfatfsHandle, 10000)) {
                System_printf("No USB drive present, logger not initialized\n"); System_flush();
                stopLogging();
                return -1;
            }
            break;
        case SDCARD:
            SDSPI_Params_init(&sdspiParams);
            sdspiHandle = SDSPI_open(Board_SDSPI0, 0, &sdspiParams);
            if (sdspiHandle == NULL) {
                System_printf("Error starting the SD Drive\n"); System_flush();
            }
            else {
                System_printf("SD Drive mounted\n"); System_flush();
            }
            break;
        default:
            return -1;
    }
    Task_Params_init(&taskParams);
    taskParams.stackSize = DEFAULTTASKSTACKSIZE*8;
    taskParams.priority = 1;
    taskParams.arg0 = SECONDS2TICKS(5);
    taskParams.arg1 = (UArg)this;
    taskParams.stack = &taskLogInfoStack;
    taskLogInfoHandle = Task_create((Task_FuncPtr)taskLogInfo, &taskParams, NULL);
    if (taskLogInfoHandle == NULL) {
        System_printf("Error: Failed to create logging test\n");
    }

    return -1;
}
int Protocol_MSS::stopLogging() {
    /* Stopping the SDCard */
    if(taskLogInfoHandle) {
        Task_delete(&taskLogInfoHandle);
    }
    if(sdspiHandle){
        SDSPI_close(sdspiHandle);
        sdspiHandle = NULL;
        System_printf("SD CARD unmounted\n");System_flush();
    }
    /* Stopping the USB Drive */
    if(usbmschfatfsHandle) {
        USBMSCHFatFs_close(usbmschfatfsHandle);
        usbmschfatfsHandle = NULL;
        System_printf("USB Stick unmounted\n");System_flush();
    }
    return -1;
}

