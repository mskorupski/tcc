/*
 * imu.cpp
 *
 *  Created on: 19 de jun de 2019
 *      Author: matheus
 */

#include <imu.hpp>
#include <driverlib/eeprom.h>

ThreeAxisMeas q2euler(const Quaternion& q) {
    ThreeAxisMeas ret;
    ret.yaw = atan2(2*(q.q0*q.q1+q.q2*q.q3),q.q0*q.q0-q.q1*q.q1-q.q2*q.q2+q.q3*q.q3);
    ret.pitch = -asin(2*q.q1*q.q3-2*q.q0*q.q2);
    ret.roll = atan2(2*(q.q0*q.q3+q.q1*q.q2),q.q0*q.q0+q.q1*q.q1-q.q2*q.q2-q.q3*q.q3);
    return ret;
}

ThreeAxisMeas q2ned(const Quaternion& q, const ThreeAxisMeas& acceleration) {
    ThreeAxisMeas ret;
    Quaternion a;
    a = acceleration.xyz;
    ret = q*a*conj(q);
    return ret;
}

IMU::IMU(float refresh_ms):
refresh_ms(refresh_ms), initialized(false)
{
    rotationQuaternion = {1.0f,0.0f,0.0f,0.0f};
    accelScaleFactor_x = 1.0;
    accelScaleFactor_y = 1.0;
    accelScaleFactor_z = 1.0;
    magScaleFactor_x = 1.0f;
    magScaleFactor_y = 1.0f;
    magScaleFactor_z = 1.0f;

    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(Board_I2C0, &i2cParams);
    if (i2c == NULL) {
        System_abort("IMU: Error Initializing I2C@IMU\n");
    }
    #ifdef DEBUG_SYSTEM_PRINTS
    else {
        System_printf("IMU: I2C Initialized!\n");
        System_flush();
    }
    #endif
    i2cTransaction.slaveAddress = MPU9250_ADDRESS;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.readBuf = rxBuffer;

}

IMU::~IMU()
{
    // TODO Auto-generated destructor stub
}

float IMU::getHeading(){
    return -atan2(2*(rotationQuaternion.q1*rotationQuaternion.q2+rotationQuaternion.q3*rotationQuaternion.q0),
                 rotationQuaternion.q0*rotationQuaternion.q0-rotationQuaternion.q3*rotationQuaternion.q3-rotationQuaternion.q2*rotationQuaternion.q2+rotationQuaternion.q1*rotationQuaternion.q1);
}

ImuSensorsMeas IMU::getLastMeasurement(){
    return lastMeasurement;
}

ThreeAxisMeas IMU::getNedAcceleration(){
    return q2ned(rotationQuaternion, lastMeasurement.accelerometer);
}

ThreeAxisMeas IMU::getEulerAngles() {
    return q2euler(rotationQuaternion);
}

Quaternion IMU::getRotationQuaternion() {
    return rotationQuaternion;
}

Quaternion qDot;
Quaternion gyro;
Quaternion mag;
Quaternion acc;
Quaternion h;
Quaternion b = {0.0f,0.0f,0.0f,0.0f};
Quaternion s;
void imuGradientDescent(const ImuSensorsMeas &newMeas, Quaternion& q, float Ts, float beta){
    gyro = newMeas.gyroscope.xyz;
    mag  = newMeas.magnetometer.xyz;
    acc  = newMeas.accelerometer.xyz;

    qDot = q*gyro*0.5f;

    if(acc){
        acc = versor(acc);
        if(mag){
            mag = versor(mag);
            h = q*mag*conj(q);
            b.q1 = sqrt(h.q1*h.q1 + h.q2*h.q2)/2.0f;
            b.q3 = h.q3/2;

            s.q0 = -2.0f*q.q2 * (2.0f * q.q1*q.q3 - 2.0f*q.q0*q.q2 - acc.q1) + 2.0f*q.q1 * (2.0f * q.q0*q.q1 + 2.0f*q.q2*q.q3 - acc.q2) - 2.0f*b.q3 * q.q2 * (2.0f*b.q1 * (0.5f - q.q2*q.q2 - q.q3*q.q3) + 2.0f*b.q3 * (q.q1*q.q3 - q.q0*q.q2) - mag.q1) + (-2.0f*b.q1 * q.q3 + 2.0f*b.q3 * q.q1) * (2.0f*b.q1 * (q.q1*q.q2 - q.q0*q.q3) + 2.0f*b.q3 * (q.q0*q.q1 + q.q2*q.q3) - mag.q2) + 2.0f*b.q1 * q.q2 * (2.0f*b.q1 * (q.q0*q.q2 + q.q1*q.q3) + 2.0f*b.q3 * (0.5f - q.q1*q.q1 - q.q2*q.q2) - mag.q3);
            s.q1 = 2.0f*q.q3 * (2.0f * q.q1*q.q3 - 2.0f*q.q0*q.q2 - acc.q1) + 2.0f*q.q0 * (2.0f * q.q0*q.q1 + 2.0f*q.q2*q.q3 - acc.q2) - 4.0f * q.q1 * (1 - 2.0f * q.q1*q.q1 - 2.0f * q.q2*q.q2 - acc.q3) + 2.0f*b.q3 * q.q3 * (2.0f*b.q1 * (0.5f - q.q2*q.q2 - q.q3*q.q3) + 2.0f*b.q3 * (q.q1*q.q3 - q.q0*q.q2) - mag.q1) + (2.0f*b.q1 * q.q2 + 2.0f*b.q3 * q.q0) * (2.0f*b.q1 * (q.q1*q.q2 - q.q0*q.q3) + 2.0f*b.q3 * (q.q0*q.q1 + q.q2*q.q3) - mag.q2) + (2.0f*b.q1 * q.q3 - 4.0f*b.q3 * q.q1) * (2.0f*b.q1 * (q.q0*q.q2 + q.q1*q.q3) + 2.0f*b.q3 * (0.5f - q.q1*q.q1 - q.q2*q.q2) - mag.q3);
            s.q2 = -2.0f*q.q0 * (2.0f * q.q1*q.q3 - 2.0f*q.q0*q.q2 - acc.q1) + 2.0f*q.q3 * (2.0f * q.q0*q.q1 + 2.0f*q.q2*q.q3 - acc.q2) - 4.0f * q.q2 * (1 - 2.0f * q.q1*q.q1 - 2.0f * q.q2*q.q2 - acc.q3) + (-4.0f*b.q1 * q.q2 - 2.0f*b.q3 * q.q0) * (2.0f*b.q1 * (0.5f - q.q2*q.q2 - q.q3*q.q3) + 2.0f*b.q3 * (q.q1*q.q3 - q.q0*q.q2) - mag.q1) + (2.0f*b.q1 * q.q1 + 2.0f*b.q3 * q.q3) * (2.0f*b.q1 * (q.q1*q.q2 - q.q0*q.q3) + 2.0f*b.q3 * (q.q0*q.q1 + q.q2*q.q3) - mag.q2) + (2.0f*b.q1 * q.q0 - 4.0f*b.q3 * q.q2) * (2.0f*b.q1 * (q.q0*q.q2 + q.q1*q.q3) + 2.0f*b.q3 * (0.5f - q.q1*q.q1 - q.q2*q.q2) - mag.q3);
            s.q3 = 2.0f*q.q1 * (2.0f * q.q1*q.q3 - 2.0f*q.q0*q.q2 - acc.q1) + 2.0f*q.q2 * (2.0f * q.q0*q.q1 + 2.0f*q.q2*q.q3 - acc.q2) + (-4.0f*b.q1 * q.q3 + 2.0f*b.q3 * q.q1) * (2.0f*b.q1 * (0.5f - q.q2*q.q2 - q.q3*q.q3) + 2.0f*b.q3 * (q.q1*q.q3 - q.q0*q.q2) - mag.q1) + (-2.0f*b.q1 * q.q0 + 2.0f*b.q3 * q.q2) * (2.0f*b.q1 * (q.q1*q.q2 - q.q0*q.q3) + 2.0f*b.q3 * (q.q0*q.q1 + q.q2*q.q3) - mag.q2) + 2.0f*b.q1 * q.q1 * (2.0f*b.q1 * (q.q0*q.q2 + q.q1*q.q3) + 2.0f*b.q3 * (0.5f - q.q1*q.q1 - q.q2*q.q2) - mag.q3);
        } else {
            s.q0 = 4.0f*q.q0 * q.q2*q.q2 + 2.0f*q.q2 * acc.q1 + 4.0f*q.q0 * q.q1*q.q1 - 2.0f*q.q1 * acc.q2;
            s.q1 = 4.0f*q.q1 * q.q3*q.q3 - 2.0f*q.q3 * acc.q1 + 4.0f * q.q0*q.q0 * q.q1 - 2.0f*q.q0 * acc.q2 - 4.0f*q.q1 + 8.0f*q.q1 * q.q1*q.q1 + 8.0f*q.q1 * q.q2*q.q2 + 4.0f*q.q1 * acc.q3;
            s.q2 = 4.0f * q.q0*q.q0 * q.q2 + 2.0f*q.q0 * acc.q1 + 4.0f*q.q2 * q.q3*q.q3 - 2.0f*q.q3 * acc.q2 - 4.0f*q.q2 + 8.0f*q.q2 * q.q1*q.q1 + 8.0f*q.q2 * q.q2*q.q2 + 4.0f*q.q2 * acc.q3;
            s.q3 = 4.0f * q.q1*q.q1 * q.q3 - 2.0f*q.q1 * acc.q1 + 4.0f * q.q2*q.q2 * q.q3 - 2.0f*q.q2 * acc.q2;
        }

        s = versor(s);

        qDot = qDot-beta*s;
    }

    q = versor(q + qDot*Ts);
}

float IMU::getLastTemperature(){
    return lastTemperature;
}

void IMU::updateRotationQuaternion(){
    imuGradientDescent(lastMeasurement, rotationQuaternion, refresh_ms/1000.0f);
}


int IMU::calibrateGyroscope(){
    float g[3];
    double D[3] = {0,0,0};

    int old__gyroRange = __gyroRange;
    int old__bandwidth = __bandwidth;
    uint8_t old_srd = _srd;
    setGyroRange(250);
    setDlpfBandwidth(20);
    setSrd(19);

    int nSamples = 250;

    for (int i=0; i < nSamples; ++i) {
       readGyroscopeRaw(g);
       D[0] += (g[0]*gyroSens)/((double)nSamples);
       D[1] += (g[1]*gyroSens)/((double)nSamples);
       D[2] += (g[2]*gyroSens)/((double)nSamples);
       delay_ms(5);
    }
    gyroBias_x = (float)D[0];
    gyroBias_y = (float)D[1];
    gyroBias_z = (float)D[2];

    // restore range, bandwidth, and srd values
    setGyroRange(old__gyroRange);
    setDlpfBandwidth(old__bandwidth);
    setSrd(old_srd);

    EEPROMProgramNonBlocking(*(uint32_t*)(&gyroBias_x),CALIB_BIAS_GYRO_X);
    EEPROMProgramNonBlocking(*(uint32_t*)(&gyroBias_y),CALIB_BIAS_GYRO_Y);
    EEPROMProgramNonBlocking(*(uint32_t*)(&gyroBias_z),CALIB_BIAS_GYRO_Z);
    return 0;
}

int IMU::calibrateAccelerometer(){
    float amin[3], amax[3], a[3];
    double D[3] = {0,0,0};
    float sf[3] = {0.0f, 0.0f, 0.0f};
    float offset[3] = {0.0f, 0.0f, 0.0f};

    int old__accelRange = __accelRange;
    int old__bandwidth = __bandwidth;
    uint8_t old_srd = _srd;
    setAccelRange(2);
    setDlpfBandwidth(20);
    setSrd(19);

    int nSamples = 250;

    for (int kt=0; kt<6; ++kt) {
        System_printf("Please, change the axis\n"); System_flush();
        delay_ms(5000);
        System_printf("Calibrating axis %d\n",(kt+1)); System_flush();
        for (int jt=0; jt < nSamples; ++jt) {
        readAccelerometerRaw(a);
        D[0] += a[0]*accSens/(double)nSamples;
        D[1] += a[1]*accSens/(double)nSamples;
        D[2] += a[2]*accSens/(double)nSamples;
        delay_ms(5);
        }
        for (int it=0; it<3; ++it){
            if(abs(D[it]) > 9.0){
                sf[it] += (float)(abs(D[it])/2.0/G);
            } else {
                offset[it] += (float)(D[it]/4.0);
            }
            D[it] = 0.0;
        }
    }
    accelBias_x = offset[0];
    accelBias_y = offset[1];
    accelBias_z = offset[2];
    accelScaleFactor_x = sf[0];
    accelScaleFactor_y = sf[1];
    accelScaleFactor_z = sf[2];

    // restore range, bandwidth, and srd values
    setAccelRange(old__accelRange);
    setDlpfBandwidth(old__bandwidth);
    setSrd(old_srd);

    EEPROMProgramNonBlocking(*(uint32_t*)(&accelBias_x),CALIB_BIAS_ACC_X);
    EEPROMProgramNonBlocking(*(uint32_t*)(&accelBias_y),CALIB_BIAS_ACC_Y);
    EEPROMProgramNonBlocking(*(uint32_t*)(&accelBias_z),CALIB_BIAS_ACC_Z);
    EEPROMProgramNonBlocking(*(uint32_t*)(&accelScaleFactor_x),CALIB_SF_ACC_X);
    EEPROMProgramNonBlocking(*(uint32_t*)(&accelScaleFactor_y),CALIB_SF_ACC_Y);
    EEPROMProgramNonBlocking(*(uint32_t*)(&accelScaleFactor_z),CALIB_SF_ACC_Z);

    return 0;
}

int IMU::calibrateMagnetometer(){
    float mmax[3], mmin[3], m[3];
    float avg;

    System_printf("Calibrating Magnetometer in 5 seconds\n"); System_flush();
    delay_ms(5000);

    uint8_t old_srd = _srd;
    int old__accelRange = __accelRange;
    int old__gyroRange = __gyroRange;
    setGyroRange(2000);
    setAccelRange(16);
    //just in case the user rotate it too fast

    setSrd(19);

    readMagnetometerRaw(mmin);
    readMagnetometerRaw(mmax);
    mmin[0]*=magSens_x;
    mmin[1]*=magSens_y;
    mmin[2]*=magSens_z;
    mmax[0]*=magSens_x;
    mmax[1]*=magSens_y;
    mmax[2]*=magSens_z;

      for (int jt = 0; jt < 15000; ++jt) {
        readMagnetometerRaw(m);
        m[0]*=magSens_x;
        m[1]*=magSens_y;
        m[2]*=magSens_z;
        for (int it = 0; it < 3; ++it) {
            if (m[it] > mmax[it] && (m[it]-mmax[it])<0.5) {
                mmax[it] = m[it];
            }
            if (m[it] < mmin[it] && (mmin[it]-m[it])<0.5) {
                mmin[it] = m[it];
            }
        }
        delay_ms(5);
      }

      // finding magnetometer bias offset
      magBias_x = (mmax[0] + mmin[0]) / 2.0f;
      magBias_y = (mmax[1] + mmin[1]) / 2.0f;
      magBias_z = (mmax[2] + mmin[2]) / 2.0f;

      // finding magnetometer scale factor
      magScaleFactor_x = (mmax[0] - mmin[0]) / 2.0f;
      magScaleFactor_y = (mmax[1] - mmin[1]) / 2.0f;
      magScaleFactor_z = (mmax[2] - mmin[2]) / 2.0f;
      avg = (magScaleFactor_x + magScaleFactor_y + magScaleFactor_z) / 3.0f;
      magScaleFactor_x = avg/magScaleFactor_x;
      magScaleFactor_y = avg/magScaleFactor_y;
      magScaleFactor_z = avg/magScaleFactor_z;

      // set the srd back to what it was
      setSrd(old_srd);
      setGyroRange(old__gyroRange);
      setAccelRange(old__accelRange);

      EEPROMProgramNonBlocking(*(uint32_t*)(&magBias_x),CALIB_BIAS_MAG_X);
      EEPROMProgramNonBlocking(*(uint32_t*)(&magBias_y),CALIB_BIAS_MAG_Y);
      EEPROMProgramNonBlocking(*(uint32_t*)(&magBias_z),CALIB_BIAS_MAG_Z);
      EEPROMProgramNonBlocking(*(uint32_t*)(&magScaleFactor_x),CALIB_SF_MAG_X);
      EEPROMProgramNonBlocking(*(uint32_t*)(&magScaleFactor_y),CALIB_SF_MAG_Y);
      EEPROMProgramNonBlocking(*(uint32_t*)(&magScaleFactor_z),CALIB_SF_MAG_Z);

      return 0;
}

int IMU::readAccelerometerRaw(float xyz[3]){
    uint8_t rxbuff[6];
    int ret = readRegisters(ACCEL_OUT, 6, rxbuff);
    if (!ret){
        xyz[0] = (float)(int16_t)((rxbuff[0] << 8) | rxbuff[1]); //little endian
        xyz[1] = (float)(int16_t)((rxbuff[2] << 8) | rxbuff[3]);
        xyz[2] = (float)(int16_t)((rxbuff[4] << 8) | rxbuff[5]);
    }
    return ret;
}

int IMU::readGyroscopeRaw(float xyz[3]){
    uint8_t rxbuff[6];
    int ret = readRegisters(GYRO_OUT, 6, rxbuff);
    if (!ret){
        xyz[0] = (float)(int16_t)((rxbuff[0] << 8) | rxbuff[1]); //little endian
        xyz[1] = (float)(int16_t)((rxbuff[2] << 8) | rxbuff[3]);
        xyz[2] = (float)(int16_t)((rxbuff[4] << 8) | rxbuff[5]);
    }
    return ret;
}

int IMU::readMagnetometerRaw(float xyz[3]){
    uint8_t rxbuff[6];
    int ret = readRegisters(EXT_SENS_DATA_00, 6, rxbuff);
    if (!ret){
        xyz[0] = (float)(int16_t)((rxbuff[1] << 8) | rxbuff[0]); //big endian
        xyz[1] = (float)(int16_t)((rxbuff[3] << 8) | rxbuff[2]);
        xyz[2] = (float)(int16_t)((rxbuff[5] << 8) | rxbuff[4]);
    }
    return ret;
}

int IMU::readSensor() {
    uint8_t rbuff[21];
    int16_t convertedReg[10];
    if (!readRegisters(ACCEL_OUT, 21, rbuff)) {
        convertedReg[0] = (((int16_t)rbuff[0]) << 8) | rbuff[1]; //little endian
        convertedReg[1] = (((int16_t)rbuff[2]) << 8) | rbuff[3];
        convertedReg[2] = (((int16_t)rbuff[4]) << 8) | rbuff[5];
        convertedReg[3] = (((int16_t)rbuff[6]) << 8) | rbuff[7];
        convertedReg[4] = (((int16_t)rbuff[8]) << 8) | rbuff[9];
        convertedReg[5] = (((int16_t)rbuff[10]) << 8) | rbuff[11];
        convertedReg[6] = (((int16_t)rbuff[12]) << 8) | rbuff[13];
        convertedReg[7] = (((int16_t)rbuff[15]) << 8) | rbuff[14]; //big endian
        convertedReg[8] = (((int16_t)rbuff[17]) << 8) | rbuff[16];
        convertedReg[9] = (((int16_t)rbuff[19]) << 8) | rbuff[18];
        lastMeasurement.accelerometer.x =   ((float)(convertedReg[0]) * accSens - accelBias_x)*accelScaleFactor_x;
        lastMeasurement.accelerometer.y =   ((float)(convertedReg[1]) * accSens - accelBias_y)*accelScaleFactor_y;
        lastMeasurement.accelerometer.z =   ((float)(convertedReg[2]) * accSens - accelBias_z)*accelScaleFactor_z;
        lastMeasurement.gyroscope.x =        (float)(convertedReg[4]) * gyroSens  - gyroBias_x;
        lastMeasurement.gyroscope.y =        (float)(convertedReg[5]) * gyroSens  - gyroBias_y;
        lastMeasurement.gyroscope.z =        (float)(convertedReg[6]) * gyroSens  - gyroBias_z;
        lastMeasurement.magnetometer.x =    ((float)(convertedReg[8]) * magSens_y  - magBias_y)*magScaleFactor_y;
        lastMeasurement.magnetometer.y =    ((float)(convertedReg[7]) * magSens_x  - magBias_x)*magScaleFactor_x;
        lastMeasurement.magnetometer.z =   -((float)(convertedReg[9]) * magSens_z  - magBias_z)*magScaleFactor_z;
//        lastMeasurement.accelerometer.y =   ((float)(convertedReg[0]) * accSens - accelBias_x)*accelScaleFactor_x;
//        lastMeasurement.accelerometer.x =   ((float)(convertedReg[1]) * accSens - accelBias_y)*accelScaleFactor_y;
//        lastMeasurement.accelerometer.z =  -((float)(convertedReg[2]) * accSens - accelBias_z)*accelScaleFactor_z;
//        lastMeasurement.gyroscope.y =        (float)(convertedReg[4]) * gyroSens  - gyroBias_x;
//        lastMeasurement.gyroscope.x =        (float)(convertedReg[5]) * gyroSens  - gyroBias_y;
//        lastMeasurement.gyroscope.z =       -(float)(convertedReg[6]) * gyroSens  - gyroBias_z;
//        lastMeasurement.magnetometer.y =    ((float)(convertedReg[8]) * magSens_y  - magBias_y)*magScaleFactor_y;
//        lastMeasurement.magnetometer.x =    ((float)(convertedReg[7]) * magSens_x  - magBias_x)*magScaleFactor_x;
//        lastMeasurement.magnetometer.z =    ((float)(convertedReg[9]) * magSens_z  - magBias_z)*magScaleFactor_z;
        lastTemperature = ((((float) convertedReg[3]) - roomTempOffset)/tempSens) + roomTempOffset;

      return 0;
    }
    return -1;
}

#include <string.h>
int IMU::readRegisters(uint8_t subAddress, uint8_t count, uint8_t* dest){
    int ret;
    txBuffer[0] = subAddress;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readCount = count;
    ret = I2C_transfer(i2c,&i2cTransaction);
    if (!ret){
        System_printf("Unsuccessful transference at I2C0!\n");
        System_flush();
    } else {
        memcpy(dest,rxBuffer,count);
    }
    return 0;
}


int IMU::writeRegister(uint8_t subAddress, uint8_t data){
    int ret;
    txBuffer[0] = subAddress;
    txBuffer[1] = data;
    i2cTransaction.writeCount = 2;
    i2cTransaction.readCount = 0;
    ret = I2C_transfer(i2c,&i2cTransaction);
    if (!ret){
        System_printf("Unsuccessful transference at I2C0!\n");
        System_flush();
    }
    return 0;
}

int IMU::writeAK8963Register(uint8_t subAddress, uint8_t data){
    int ret = writeRegister(I2C_SLV0_ADDR,AK8963_ADDRESS);
    ret |= writeRegister(I2C_SLV0_REG,subAddress);
    ret |= writeRegister(I2C_SLV0_DO,data);
    ret |= writeRegister(I2C_SLV0_CTRL,I2C_SLV0_EN | (uint8_t)1);
    return ret;
}

int IMU::readAK8963Registers(uint8_t subAddress, uint8_t count, uint8_t* dest){
    int ret = writeRegister(I2C_SLV0_ADDR,AK8963_ADDRESS | I2C_READ_FLAG);
    ret |= writeRegister(I2C_SLV0_REG,subAddress);
    ret |= writeRegister(I2C_SLV0_CTRL,I2C_SLV0_EN | count);
    return ret | readRegisters(EXT_SENS_DATA_00,count,dest);
}

int IMU::setSrd(uint8_t srd) {
    uint8_t magBuff[10];
    /* setting the sample rate divider to 19 to facilitate setting up magnetometer */
    if(writeRegister(SMPDIV,19) < 0){ // setting the sample rate divider
      return -1;
    }
    writeAK8963Register(AK8963_CNTL1,AK8963_PWR_DOWN);
    delay_ms(100);
    writeAK8963Register(AK8963_CNTL1,(srd > 9)?AK8963_CNT_MEAS1:AK8963_CNT_MEAS2);
    delay_ms(100);
    readAK8963Registers(AK8963_HXL,7,magBuff);
    if(writeRegister(SMPDIV,srd) < 0){
      return -4;
    }
    _srd = srd;
    return 1;
}

void IMU::init(){
    if(initialized){
        return;
    }
    uint8_t magBuff[10];
    uint32_t eepromBuff[15];
    writeRegister(PWR_MGMNT_1,CLOCK_SEL_PLL);
    writeRegister(USER_CTRL,I2C_MST_EN);
    writeRegister(I2C_MST_CTRL,I2C_MST_CLK);
    writeAK8963Register(AK8963_CNTL1,AK8963_PWR_DOWN);
    writeRegister(PWR_MGMNT_1,PWR_RESET);
    delay_ms(1);
    writeAK8963Register(AK8963_CNTL2,AK8963_RESET);
    writeRegister(PWR_MGMNT_1,CLOCK_SEL_PLL);
    writeRegister(PWR_MGMNT_2,SEN_ENABLE);

    setAccelRange(2);
    setGyroRange(2000);
    setDlpfBandwidth(184);

    _srd = 0;
    writeRegister(USER_CTRL,I2C_MST_EN);
    writeRegister(I2C_MST_CTRL,I2C_MST_CLK);
    writeAK8963Register(AK8963_CNTL1,AK8963_PWR_DOWN);
    delay_ms(100);
    writeAK8963Register(AK8963_CNTL1,AK8963_FUSE_ROM);
    delay_ms(100);
    readAK8963Registers(AK8963_ASA,3,magBuff);

//    from RM-MPU-9250A-00, pages 50 and 53
//    maximum read Magnetic flux density is +-32760, which equals to +-4912 uTesla
//    The sensitivity adjustment is done by the equation below:
//    Hadj=H*((ASA-128)*0.5/128+1)
    magSens_x = ((((float)magBuff[0]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;
    magSens_y = ((((float)magBuff[1]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;
    magSens_z = ((((float)magBuff[2]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;

    writeAK8963Register(AK8963_CNTL1,AK8963_PWR_DOWN);
    delay_ms(100);
    writeAK8963Register(AK8963_CNTL1,AK8963_CNT_MEAS2);
    delay_ms(100);
    writeRegister(PWR_MGMNT_1,CLOCK_SEL_PLL);
    readAK8963Registers(AK8963_HXL,7,magBuff);

//    read last calibration from eeprom
//    EEPROMRead((uint32_t*)eepromBuff,CALIB_BIAS_GYRO_X,sizeof(eepromBuff)/sizeof(eepromBuff));
//    accelBias_x = *(float*)(eepromBuff+0);
//    accelBias_y = *(float*)(eepromBuff+1);
//    accelBias_z = *(float*)(eepromBuff+2);
//    accelScaleFactor_x = *(float*)(eepromBuff+3);
//    accelScaleFactor_y = *(float*)(eepromBuff+4);
//    accelScaleFactor_z = *(float*)(eepromBuff+5);
//    gyroBias_x = *(float*)(eepromBuff+6);
//    gyroBias_y = *(float*)(eepromBuff+7);
//    gyroBias_z = *(float*)(eepromBuff+8);
//    magBias_x = *(float*)(eepromBuff+9);
//    magBias_y = *(float*)(eepromBuff+10);
//    magBias_z = *(float*)(eepromBuff+11);
//    magScaleFactor_x = *(float*)(eepromBuff+12);
//    magScaleFactor_y = *(float*)(eepromBuff+13);
//    magScaleFactor_z = *(float*)(eepromBuff+14);

    accelBias_x = 0.281402284f;
    accelBias_y = 0.602902439;
    accelBias_z = 0.473914225f;
    accelScaleFactor_x = 0.998084772f;
    accelScaleFactor_y = 0.999443316f;
    accelScaleFactor_z = 1.014884734f;
    gyroBias_x = -0.039199930625000;
    gyroBias_y = -0.002495819352500;
    gyroBias_z = 0.006261320085000;
//    magBias_x = 39.265176960000002;
//    magBias_y = 9.875942228000000;
//    magBias_z = 3.333829592000000;
////    magBias_x = 15.265176960000002;
////    magBias_y = 9.875942228000000;
////    magBias_z = -4.333829592000000;
//    magScaleFactor_x = 0.949309445200000;
//    magScaleFactor_y = 1.043940341800000;
//    magScaleFactor_z = 1.022066793400000;
    magBias_x = 16.265176960000002;
    magBias_y = 14.2816849;
    magBias_z = -8.88388252;
//    magBias_x = 15.265176960000002;
//    magBias_y = 9.875942228000000;
//    magBias_z = -4.333829592000000;
    magScaleFactor_x = 0.743827164;
    magScaleFactor_y = 1.59075904;
    magScaleFactor_z = 0.973737419;

    initialized = true;
}

bool IMU::getInitialized(){
    return initialized;
}

int IMU::setAccelRange(int range) {
  switch(range) {
    case 2: {
      if(writeRegister(ACCEL_CONFIG,ACCEL_FS_SEL_2G) < 0){
        return -1;
      }
      break;
    }
    case 4: {
      if(writeRegister(ACCEL_CONFIG,ACCEL_FS_SEL_4G) < 0){
        return -1;
      }
      break;
    }
    case 8: {
      if(writeRegister(ACCEL_CONFIG,ACCEL_FS_SEL_8G) < 0){
        return -1;
      }
      break;
    }
    case 16: {
      if(writeRegister(ACCEL_CONFIG,ACCEL_FS_SEL_16G) < 0){
        return -1;
      }
        break;
    }
    default:
        return -1;
  }
  accSens = G * range/32767.5f;
  __accelRange = range;
  return 1;
}

int IMU::setGyroRange(int range) {
    switch(range) {
      case 250: {
        if(writeRegister(GYRO_CONFIG,GYRO_FS_SEL_250DPS) < 0){
          return -1;
        }
        break;
      }
      case 500: {
        if(writeRegister(GYRO_CONFIG,GYRO_FS_SEL_500DPS) < 0){
          return -1;
        }
        break;
      }
      case 1000: {
        if(writeRegister(GYRO_CONFIG,GYRO_FS_SEL_1000DPS) < 0){
          return -1;
        }
        break;
      }
      case 2000: {
        if(writeRegister(GYRO_CONFIG,GYRO_FS_SEL_2000DPS) < 0){
          return -1;
        }
        break;
      }
      default:
          return -1;
    }
    gyroSens = DEG2RAD((range/32767.5f));
    __gyroRange = range;
        return 1;
}


int IMU::setDlpfBandwidth(int bandwidth) {
    switch(bandwidth) {
      case 184: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_184) < 0){ // setting accel bandwidth to 184Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_184) < 0){ // setting gyro bandwidth to 184Hz
          return -2;
        }
        break;
      }
      case 192: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_92) < 0){ // setting accel bandwidth to 92Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_92) < 0){ // setting gyro bandwidth to 92Hz
          return -2;
        }
        break;
      }
      case 41: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_41) < 0){ // setting accel bandwidth to 41Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_41) < 0){ // setting gyro bandwidth to 41Hz
          return -2;
        }
        break;
      }
      case 20: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_20) < 0){ // setting accel bandwidth to 20Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_20) < 0){ // setting gyro bandwidth to 20Hz
          return -2;
        }
        break;
      }
      case 10: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_10) < 0){ // setting accel bandwidth to 10Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_10) < 0){ // setting gyro bandwidth to 10Hz
          return -2;
        }
        break;
      }
      case 5: {
        if(writeRegister(ACCEL_CONFIG2,ACCEL_DLPF_5) < 0){ // setting accel bandwidth to 5Hz
          return -1;
        }
        if(writeRegister(CONFIG,GYRO_DLPF_5) < 0){ // setting gyro bandwidth to 5Hz
          return -2;
        }
        break;
      }
    }
    __bandwidth = bandwidth;
    return 1;
}
